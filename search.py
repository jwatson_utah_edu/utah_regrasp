# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """
    
    "*** YOUR CODE HERE ***"
    # ~~ Example Usage and Problem Format ~~
    # problem.getStartState()    
    # prob.getSuccessors( node ) # fetch successors
    # [((5, 4), 'South', 1), ((4, 5), 'West', 1)]
    # (successor, action, stepCost)
    # problem.isGoalState( node )
    
    startNode = problem.getStartState() # fetch the starting node
    
    def DFS_recur(prob, node):
        """ Recursive Depth First Search, does the work for this agent """
        # 'DFS_recur' is a functor, it stores the visited nodes and the current path within the function itself
        # These are two less items that have to be included in the function params
        
        DFS_recur.visited.add( node ) # log this node as visited
        
        neighbors = prob.getSuccessors( node ) # find all the successors of the current node
        
        for nghbr in neighbors: # for each successor of this node, do
            if not nghbr[0] in DFS_recur.visited: # if node has not been visited before, then
                if prob.isGoalState( nghbr[0] ): # if successor is goal, then
                    DFS_recur.path.push( nghbr[1] ) # push onto current path
                    return True # return success
                else: # else the successor has not been visited and is not the goal, so
                    DFS_recur.path.push( nghbr[1] ) # tentatively push successor onto current path
                    if not DFS_recur(prob, nghbr[0]): # recur on the successor, if goal was not found at a greater depth of branch, then
                        DFS_recur.path.pop() # remove successor from the current path
                    else: # else goal was found at a greater depth on the branch
                        return True # return True back up to top to end the search
                        
        return False # All successors expanded and explored without success, this branch fails
                    
    # Init functor
    DFS_recur.visited = set([]) # the set of visited nodes
    DFS_recur.path = util.Stack() # the path for this Agent, problem authors were nice enough to provice a Stack class
    
    DFS_recur(problem, startNode) # invoke search beginning with the start
    
    return DFS_recur.path.list # Return the path found by 'DFS_recur'
    # NOTE: If the goal does not exist or is unreachable by the search, function will return an empty list!

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"    
    startNode = problem.getStartState() # fetch the starting node
       
    def is_in_path_queue(pathQ, key): # find a node in our custom queue
        for item in pathQ.list: # for item in the queue
            if item[0] == key: # If the current item is equal to key, then
                return True # match found, return true
        return False # else iterated across items without match, return false
    
    def BFS_helper(prob, node):
        """ Does the work of Breadth First Search """
        # Each item in the frontier has [node,[PATHTONODE]]
        BFS_helper.frontier.push( [node, []] ) # push the root node onto the frontier with an empty path
        
        while not BFS_helper.frontier.isEmpty(): # while there are still nodes on the frontier queue
            parent = BFS_helper.frontier.pop() # get the first item in the queue
            BFS_helper.visited.add( parent[0] ) # mark this item as visited
            if problem.isGoalState( parent[0] ): # if Agent has arrived at the goal, then
                return parent[1] # return the path that got it there
            neighbors = prob.getSuccessors( parent[0] ) # find all the successors of the current node
            for nghbr in neighbors: # for each successor of this node, do
                # if the successor has not been visited and is also not presently in the frontier queue
                if not nghbr[0] in BFS_helper.visited and not is_in_path_queue(BFS_helper.frontier, nghbr[0]):
                    pathTo = parent[1][:] # copy the path to the parent
                    pathTo.append( nghbr[1] ) # append the direction to this node
                    BFS_helper.frontier.push( [nghbr[0], pathTo] ) # push the successor onto the frontier queue
        return [False] # search expanded all nodes without result
    
    # ~ init Functor ~
    BFS_helper.frontier = util.Queue()
    BFS_helper.visited = set([]) # the set of visited nodes
    
    return BFS_helper(problem, startNode) # invoke the search on the starting node

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    startNode = problem.getStartState() # fetch the starting node
    
    def UCS_helper(prob, node):
        """ Does the work of Uniform Cost Search """
        # Each item in the frontier has [node,[PATHTONODE]]
        UCS_helper.frontier.push( [node, []] , 0 ) # push the root node onto the frontier with an empty path
        #                         item       , value
        
        while not UCS_helper.frontier.isEmpty(): # while there are still nodes on the frontier queue
            temp = UCS_helper.frontier.pop() # get the first item in the queue
            while temp[0] in UCS_helper.popped: # if this node has been popped before, it was at a lower cost
                temp = UCS_helper.frontier.pop() # get the next item in the queue
            # popped a node that has not been popped before
            UCS_helper.popped.add( temp[0] ) # add to set of popped nodes
            parent = temp # good next node, store it
            UCS_helper.visited.add( parent[0] ) # mark this item as visited
            if problem.isGoalState( parent[0] ): # if Agent has arrived at the goal, then
                return parent[1] # return the path that got it there
            neighbors = prob.getSuccessors( parent[0] ) # find all the successors of the current node
            for nghbr in neighbors: # for each successor of this node, do
                # if the successor has not been visited and is also not presently in the frontier queue
                if not nghbr[0] in UCS_helper.visited: #and nghbr[0] in UCS_helper.heaped:
                #not is_in_path_pri_queue(UCS_helper.frontier, nghbr[0]):
                    pathTo = parent[1][:] # copy the path to the parent
                    pathTo.append( nghbr[1] ) # append the direction to this node
                    UCS_helper.frontier.push( [nghbr[0], pathTo], prob.getCostOfActions(pathTo) ) # push the successor onto the frontier queue
        return [False] # search expanded all nodes without result
    
    # ~ init Functor ~
    UCS_helper.frontier = util.PriorityQueue() # using a priority queue this time
    UCS_helper.visited = set([]) # the set of visited nodes
    UCS_helper.popped = set([]) # a set of popped nodes
    
    return UCS_helper(problem, startNode) # invoke the search on the starting node

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    # 'heuristic' is a function from them, call with 'heuristic(node)'

    startNode = problem.getStartState() # fetch the starting node
    
    def UCS_helper(prob, node):
        """ Does the work of Breadth First Search """
        # Each item in the frontier has [node,[PATHTONODE]]
        UCS_helper.frontier.push( [node, []] , 0 ) # push the root node onto the frontier with an empty path
        
        while not UCS_helper.frontier.isEmpty(): # while there are still nodes on the frontier queue
            temp = UCS_helper.frontier.pop() # get the first item in the queue
            while temp[0] in UCS_helper.popped: # if this node has been popped before, it was at a lower cost
                temp = UCS_helper.frontier.pop() # get the next item in the queue
            # popped a node that has not been popped before
            UCS_helper.popped.add( temp[0] ) # add to set of popped nodes
            parent = temp # good next node, store it
            UCS_helper.visited.add( parent[0] ) # mark this item as visited
            if problem.isGoalState( parent[0] ): # if Agent has arrived at the goal, then
                return parent[1] # return the path that got it there
            neighbors = prob.getSuccessors( parent[0] ) # find all the successors of the current node
            for nghbr in neighbors: # for each successor of this node, do
                # if the successor has not been visited and is also not presently in the frontier queue
                if not nghbr[0] in UCS_helper.visited: #and nghbr[0] in UCS_helper.heaped:
                #not is_in_path_pri_queue(UCS_helper.frontier, nghbr[0]):
                    pathTo = parent[1][:] # copy the path to the parent
                    pathTo.append( nghbr[1] ) # append the direction to this node
                    UCS_helper.frontier.push( [nghbr[0], pathTo], prob.getCostOfActions(pathTo) + heuristic(nghbr[0], prob) ) # push the successor onto the frontier queue
        return [False] # search expanded all nodes without result
    
    # ~ init Functor ~
    UCS_helper.frontier = util.PriorityQueue() # using a priority queue this time
    UCS_helper.visited = set([]) # the set of visited nodes
    UCS_helper.popped = set([]) # a set of popped nodes
    
    return UCS_helper(problem, startNode) # invoke the search on the starting node

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
