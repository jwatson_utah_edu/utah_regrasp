# -*- coding: utf-8 -*-
"""
pnt-in-poly.py , Built on Spyder for Python 2.7
James Watson, 2016 February
Implementation of the winding number of a polygon with respect to a point from Alciatore. Based on Alciatore's pseudocode
    David G. Alciatore and Rick Miranda.  A winding number and point-in-polygon algorithm.  Technical report, 
    Colorado State University, 1995.
URL: https://www.engr.colostate.edu/~dga/dga/papers/point_in_polygon.pdf
"""
# == Init Environment ==

# ~ PATH Changes ~ 
def localize():
    """ Add the current directory to Python path if it is not already there """
    from sys import path # I know it is bad style to load modules in function
    import os.path as os_path
    containerDir = os_path.dirname(__file__)
    if containerDir not in path:
        path.append( containerDir )

localize() # You can now load local modules!

# ~ Standard Libraries ~
import math
from math import sqrt, ceil, sin, cos, tan, atan2
from os import linesep
# ~ Special Libraries ~
import matplotlib.pyplot as plt
import numpy as np
# ~ Constants ~
EPSILON = 1e-7 # A "close enough" delta for which two number are considered equivalent in this problem
# ~~ Shortcuts and Aliases ~~
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = linesep

# ~ Helper Functions ~

def eq(op1, op2):
    """ Return true if op1 and op2 are ''close enough'' """
    return abs(op1 - op2) <= EPSILON
    
def sep(title = ""):
    """ Print a separating title card to clarify output """
    LINE = '======'
    print LINE + ' ' + title + ' ' + LINE

# == End Init ==

poly = ((0,0),(0,9),(9,9),(9,6),(6,6),(6,3),(9,3),(9,0))

insPnt1 = (  7.5 ,  7.5 )
insPnt2 = (  1.0 ,  1.0 )
outPnt1 = (  7.5 ,  4.5 )
outPnt2 = ( -1.0 ,  1.0 )

def elemw(i, iterable):
    """ Return the 'i'th index of 'iterable', wrapping to index 0 at all integer multiples of 'len(iterable)' """
    return iterable[ i % ( len(iterable) ) ]

def winding_num(point , polygon):
    """ Find the winding number of a point with respect to a polygon """
    # NOTE: Function assumes that the points of 'polygon' are ordered. Algorithm does not work if they are not
    # This algorithm is translation invariant, and can handle convex, nonconvex, and polygons with crossing sides.
    # This algorithm does NOT handle the case when the point lies ON a polygon side. For this problem it is assumed that
    #there are enough trial points to ignore this case
    # This works by shifting the point to the origin (preserving the relative position of the polygon points), and 
    #tracking how many times the positive x-axis is crossed
    w = 0
    v_i = []
    x = lambda index: elemw( index , v_i )[0] # We need wrapping indices here because the polygon is a cycle
    y = lambda index: elemw( index , v_i )[1]
    for vertex in polygon: # Shift the point to the origin, preserving its relative position to polygon points
        v_i.append( np.subtract( vertex , point ) )
    for i in range(len(v_i)): # for each of the transformed polygon points, consider segment v_i[i]-->v_i[i+1]
        if y(i) * y(i + 1) < 0: # if the segment crosses the x-axis
            r = x(i) + ( y(i) * ( x(i + 1) - x(i) ) ) / ( y(i) - y(i + 1) ) # location of x-axis crossing
            if r > 0: # positive x-crossing
                if y(i) < 0: 
                    w += 1 # CCW encirclement
                else:
                    w -= 1 #  CW encirclement
        # If one of the polygon points lies on the x-axis, we must look at the segments before and after to determine encirclement
        elif ( eq( y(i) , 0 ) ) and ( x(i) > 0 ): # v_i[i] is on the positive x-axis, leaving possible crossing
            if y(i) > 0:
                w += 0.5 # possible CCW encirclement or switchback from a failed CW crossing
            else:
                w -= 0.5 # possible CW encirclement or switchback from a failed CCW crossing
        elif ( eq( y(i + 1) , 0 ) ) and ( x(i + 1) > 0 ): # v_i[i+1] is on the positive x-axis, approaching possible crossing
            if y(i) < 0:
                w += 0.5 # possible CCW encirclement pending
            else:
                w -= 0.5 # possible  CW encirclement pending
    return w

def point_in_poly_w(point , polygon):
    return not eq( winding_num(point , polygon) , 0 )

sep('Inside Points')  
#print winding_num(insPnt1, poly)
print "Point",insPnt1,"has winding number",winding_num(insPnt1, poly),". So is it in the polygon?",point_in_poly_w(insPnt1, poly)
print "Point",insPnt2,"has winding number",winding_num(insPnt2, poly),". So is it in the polygon?",point_in_poly_w(insPnt2, poly)
sep('Outside Points')
#print winding_num(outPnt1, poly)
print "Point",outPnt1,"has winding number",winding_num(outPnt1, poly),". So is it in the polygon?",point_in_poly_w(outPnt1, poly)
print "Point",outPnt2,"has winding number",winding_num(outPnt2, poly),". So is it in the polygon?",point_in_poly_w(outPnt2, poly)