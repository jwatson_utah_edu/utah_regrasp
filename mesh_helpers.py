#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Template Version: 2016-05-10
"""
mesh_helpers.py , Built on Spyder for Python 2.7
James Watson, 2016 June
Moved all the helper functions from "mesh_general.py" to this file once I started using them in other files

"""

import os, sys           
def add_first_valid_dir_to_path(dirList):
    """ Add the first valid directory in 'dirList' to the system path """
    # In lieu of actually installing the library, just keep a list of all the places it could be in each environment
    loadedOne = False
    for drctry in dirList:
        if os.path.exists( drctry ):
            loadedOne = True
            if not drctry in sys.path:
                sys.path.append( drctry )
                print 'Loaded', str(drctry)
            else:
                print str(drctry) , 'was already in the path'
            break
    if not loadedOne:
        print "None of the specified directories were loaded"

from Queue import PriorityQueue # for ranking grasp pairs
import cPickle # Possibly MUCH faster than 'pickle'
import datetime
# ~ Special ~
from stl import mesh # https://pypi.python.org/pypi/numpy-stl/ # pip install numpy-stl
import pcl
# ~ Custom Local ~
import util

add_first_valid_dir_to_path( [ '/home/jwatson/regrasp_planning/researchenv'] )
from ResearchEnv import *
from ResearchUtils.Plotting import *
localize(__file__) # You can now load local modules!

add_first_valid_dir_to_path( [ '/home/jwatson/regrasp_planning/graphtoolkit',
                               '/media/mawglin/FILEPILE/Utah_Research/zz_regrasp_bkup/graphtoolkit'] )
from GraphToolkit import *

# = Data Structure Helpers =
    
item_at_addr = lambda structure , pTple : structure[pTple[0]][pTple[1]]
""" Return the element at structure[i][j] given the address 'tple' = (i,j) """

def eql_pair(op1,op2): 
    """ Return true if both elements of 'op1' are in 'op2', and vice-versa, otherwise return false """
    # NOTE: This function assumes that 'op1' and 'op2' are iterables with 2 elements each
    if len(op1) == len(op2) == 2:
        return (op1[0] in op2) and (op1[1] in op2) and (op2[0] in op1) and (op2[1] in op1)
    else:
        raise ValueError('eql_pair: Expected 2 iterables with length 2 each!')
        
def pair_in_list( keyPair , pairList ): 
    """ Return True if 'keyPair' is equivalent to any of 'pairList', otherwise return False """
    for pair in pairList:
        if eql_pair( keyPair , pair ):
            return True # Found a match, break and return
    return False # Searched entire list without a match, return False
    
# = End Structure =

# = Geometry Helpers =
def np_vec_proj(a,b):
    """ a projected onto b, a scalar length, using numpy """
    return np.dot(a,b) / np.linalg.norm(b) # Note that the result will be negative if the angle between a and b is > pi/2
    
def np_unit_vec(vec):
    """ Return a unit vector in the direction of 'vec', using numpy """
    return np.divide( vec , np.linalg.norm(vec) )
    
def np_angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2' """
	# http://stackoverflow.com/a/13849249/893511
    v1_u = np_unit_vec(v1)
    v2_u = np_unit_vec(v2)
    angle = np.arccos(np.dot(v1_u, v2_u))
    if np.isnan(angle):
        if (v1_u == v2_u).all():
            return 0.0
        else:
            return np.pi
    return angle

def np_add(*args):
    """ Perform 'np.add' on more than two vars """
    if len(args) > 2:
        return args[0] + np_add(*args[1:]) # Note the star operator is needed for recursive call
        #                                           The star operator unpacks the list into positional arguments
    else:
        return np.add( args[0] , args[1] )

def np_proj_vec_to_plane(vec, planeNorm):
    """ Return a vector that is the projection of 'vec' onto a plane with the normal vector 'planeNorm', using numpy """
    # URL, projection of a vector onto a plane: http://www.euclideanspace.com/maths/geometry/elements/plane/lineOnPlane/
    # NOTE: This function assumes 'vec' and 'planeNorm' are expressed in the same bases
    projDir = np_unit_vec( np.cross( np.cross( planeNorm , vec ) , planeNorm ) ) # direction of the projected vector, normalize
    projMag = np_vec_proj( vec , projDir ) # magnitude of the vector in the projected direction
    return np.multiply( projDir , projMag ) # scale the unit direction vector to the calculated magnitude and return

def bounding_points_from_hull(hull):
    """ Given a scipy.spatial.ConvexHull, return a list of the (supposedly) bounding points """
    # NOTE: Somehow the ConvexHull was not so convex?
    pntList = []
    # ConvexHull.vertices contains the indices of ConvexHull.points that are the bounding poly
    for index in hull.vertices: 
        pntList.append( hull.points[index] )
    return pntList
    
def cosd(angleDeg):
    """ Return the cosine of the angle, given in degrees """
    return cos( radians( angleDeg ) )
    
def vector_mean(*args): # This does the same thing as np.mean?
    rtnVec = []
    for index in xrange(len(args[0])): # assumes that all args are vectors of the same length
        rtnVec.append( 0 )
        for arg in args:
            rtnVec[index] += arg[index]
        #rtnVec[index] /= len(args[0]) # THIS IS THE WRONG DENOMINATOR!
        rtnVec[index] /= len(args)
    return rtnVec    

def winding_num(point , polygon):
    """ Find the winding number of a point with respect to a polygon """
    # NOTE: Function assumes that the points of 'polygon' are ordered. Algorithm does not work if they are not
    # This algorithm is translation invariant, and can handle convex, nonconvex, and polygons with crossing sides.
    # This algorithm does NOT handle the case when the point lies ON a polygon side. For this problem it is assumed that
    #there are enough trial points to ignore this case
    # This works by shifting the point to the origin (preserving the relative position of the polygon points), and 
    #tracking how many times the positive x-axis is crossed
    w = 0
    v_i = []
    x = lambda index: elemw( index , v_i )[0] # We need wrapping indices here because the polygon is a cycle
    y = lambda index: elemw( index , v_i )[1]
    for vertex in polygon: # Shift the point to the origin, preserving its relative position to polygon points
        v_i.append( np.subtract( vertex , point ) )
    for i in range(len(v_i)): # for each of the transformed polygon points, consider segment v_i[i]-->v_i[i+1]
        if y(i) * y(i + 1) < 0: # if the segment crosses the x-axis
            r = x(i) + ( y(i) * ( x(i + 1) - x(i) ) ) / ( y(i) - y(i + 1) ) # location of x-axis crossing
            if r > 0: # positive x-crossing
                if y(i) < 0: 
                    w += 1 # CCW encirclement
                else:
                    w -= 1 #  CW encirclement
        # If one of the polygon points lies on the x-axis, we must look at the segments before and after to determine encirclement
        elif ( eq( y(i) , 0 ) ) and ( x(i) > 0 ): # v_i[i] is on the positive x-axis, leaving possible crossing
            if y(i) > 0:
                w += 0.5 # possible CCW encirclement or switchback from a failed CW crossing
            else:
                w -= 0.5 # possible CW encirclement or switchback from a failed CCW crossing
        elif ( eq( y(i + 1) , 0 ) ) and ( x(i + 1) > 0 ): # v_i[i+1] is on the positive x-axis, approaching possible crossing
            if y(i) < 0:
                w += 0.5 # possible CCW encirclement pending
            else:
                w -= 0.5 # possible  CW encirclement pending
    return w

def point_in_poly_w(point , polygon):
    """ Return True if the 'polygon' contains the 'point', otherwise return False, based on the winding number """
    return not eq( winding_num(point , polygon) , 0 ) # The winding number gives the number of times a polygon encircles a point    
    
def centroid_discrete_masses(massCenters, totalMass):
    """ Return the COM for a collection of point masses 'massCenters' with a known 'totalMass' """
    centroid = [0 for i in range(len(massCenters[0][1]))] # build a zero vector the same dimensionality as the data coords
    for massPoint in massCenters: # for every mass-point pair in the data
        for i, coord in enumerate(massPoint[1]): # for every coordinate in the point
            centroid[i] += massPoint[0]/totalMass * coord # Add the coordinate scaled by it's mass distribution
    #print "Centroid:",centroid
    return centroid 

def d_point_to_segment_2d(point, segPt1, segPt2):
    """ Return the shortest (perpendicular) distance between 'point' and a line segment defined by 'segPt1' and 'segPt2' """
    # URL: http://mathworld.wolfram.com/Point-LineDistance2-Dimensional.html
    return abs( (segPt2[0]-segPt1[0])*(segPt1[1]-point[1]) - (segPt1[0]-point[0])*(segPt2[1]-segPt1[1]) ) / sqrt( (segPt2[0]-segPt1[0])**2 + (segPt2[1]-segPt1[1])**2 )

def intersect_seg_2D( seg1 , seg2 ):
    """ Return true if line segments 'seg1' and 'seg2' intersect, otherwise false """
    # URL: http://www-cs.ccny.cuny.edu/~wolberg/capstone/intersection/Intersection%20point%20of%20two%20lines.html
    # NOTE: 'uA' and 'uB' could be used to calc intersection point if desired, see above URL
    den =   (seg2[1][1] - seg2[0][1]) * (seg1[1][0] - seg1[0][0]) - (seg2[1][0] - seg2[0][0]) * (seg1[1][1] - seg1[0][1])
    uAnum = (seg2[1][0] - seg2[0][0]) * (seg1[0][1] - seg2[0][1]) - (seg2[1][1] - seg2[0][1]) * (seg1[0][0] - seg2[0][0])
    uBnum = (seg1[1][0] - seg1[0][0]) * (seg1[0][1] - seg2[0][1]) - (seg1[1][1] - seg1[0][1]) * (seg1[0][0] - seg2[0][0])
    if den == 0:
        if eq(uAnum , 0.0) or eq(uBnum , 0.0):
            return True
        else:
            return False
    else:
        uA = uAnum * 1.0 / den
        uB = uBnum * 1.0 / den
        if (uA >= 0 and uA <= 1) and (uB >= 0 and uB <= 1):
            return True
        else:
            return False

def poly2D_area_centroid(polyPoints): # totArea , centroid
    """ Return the area and centroid of a convex polygon composed of at least three 'polyPoints' """
    # http://www.mathopenref.com/coordtrianglearea.html
    # Assume that we are dealing with convex polygons (hulls), otherwise use shoelace algorithm
    # NOTE: This algorithm will produce incorrect results for a nonconvex polygon!
    A = 0 # triangle area
    triCentroids = list() # Triangle centers
    totArea = 0 # total area of the polygon
    for pntDex in range( 2 , len(polyPoints) ): # for each point in the polygon, starting with the third
        Ax = polyPoints[pntDex][0] # The point at index 0 will always be one vertex of each triangle
        Bx = polyPoints[pntDex-1][0]
        Cx = polyPoints[0][0]
        Ay = polyPoints[pntDex][1]
        By = polyPoints[pntDex-1][1]
        Cy = polyPoints[0][1]
        A = 0.5 * abs( Ax*(By-Cy) + Bx*(Cy-Ay) + Cx*(Ay-By) )
        totArea += A
        triCentroids.append( ( A , vector_mean( polyPoints[pntDex] , polyPoints[pntDex-1] , polyPoints[0] ) ) )
    return totArea , centroid_discrete_masses( triCentroids , totArea )

def eq_vec(vec1 , vec2):
    """ Return true if two vectors are equal enough, otherwise false """
    areEq = True
    for i in range(len(vec1)):
        if not eq( vec1[i] , vec2[i] ):
            areEq = False
            break
    return areEq

def np_basis_change_3D(point, origin, xBasis, yBasis, zBasis):
    """ Express a 'point' in a new basis, according to 'origin', 'xBasis', 'yBasis', 'zBasis' (all expressed in old basis) """
    offset = np.subtract( point , origin )
    # NOTE: This is probably faster as a matrix operation
    return ( np_vec_proj( offset , xBasis ) , np_vec_proj( offset , yBasis ) , np_vec_proj( offset , zBasis ) )
        
# = End Geo =

# = Plotting Helpers =
    
def plot_points_only_model(PKLpath):
    """ Plot the uniqueified points already stored in a PKL file """ # NOTE: This function assumes a points-only file exists!
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    pointsOnly = load_pkl_struct(PKLpath) # You must be aware of the structure in order to use it
    
    xs,ys,zs = split_to_components( pointsOnly )
    ax.scatter(xs, ys, zs, c='blue', marker='o', s=14)
    plot_axes_3D_mpl(ax, scale = 0.05)
    # Show the attachment point that will be used to attach to grasp points
    
    #xs,ys,zs = split_to_components( [indexFingTip] )
    #ax.scatter(xs, ys, zs, c='red', marker='+' , s=140)
    
    plt.gca().set_aspect('equal')
    plt.show()
     
def circ_spacing( dia , numPts, center = None ):
    """ Return a list of 'numPts' points equally spaced around a circle with a center at (0,0), or at 'center' if specified """
    div = 2 * pi / numPts
    circPts = []
    for pntDex in range(numPts):
        # Convert polar to Cartesian coordinates, theta = 0 is UP (radians)
        offset = ( dia/2 * sin(pntDex * div) , dia/2 * cos(pntDex * div) )
        if center:
            circPts.append( np.add( center , offset ) )
        else:
            circPts.append( offset )
    return circPts
   
# = End Plotting =

# = STL Helpers =
    
def len_mesh(mesh):
    return len(mesh.normals)
    
def tri_tuple(mesh, index):
    """ Return a tuple of (normal, Vertex0, Vertex1, Vertex2) """
    if index <= len_mesh(mesh) - 1:
        return (mesh.normals[index], mesh.v0[index], mesh.v1[index], mesh.v2[index])
    else:
        raise IndexError("tri_tuple: Index " + str(index) + " was out of bounds! Size " + str(len_mesh(mesh)) if mesh else "UNKNOWN")

def tri_centers(mesh):
    """ Return the center of a triangle, the average position of its three vertices """
    rtnPnts = []
    for i in xrange(len(mesh)):
        rtnPnts.append( vector_mean( mesh.v0[i] , mesh.v1[i] , mesh.v2[i]) )
    return rtnPnts

def stl_mesh_diagnostics(your_mesh):
    """ Print information on a mesh object generated by 'mesh.Mesh.from_file' """
    sep('Printing Diagnostics for Mesh Generated from STL File')
    print "Found", len(your_mesh.normals), "normals" # the normal vector of each facet # Found 4000 normals
    print "Found", len(your_mesh.v0), "vectors in V0" # 1st vertex of triangular facet # Found 4000 vectors in V0
    print "Found", len(your_mesh.v1), "vectors in V1" # 2st vertex of triangular facet # Found 4000 vectors in V1
    print "Found", len(your_mesh.v2), "vectors in V2" # 3st vertex of triangular facet # Found 4000 vectors in V2
    print "Found", len(your_mesh.points), "points" # points (concatenation of v0, v1 and v2 in triplets) # Found 4000 points
    print your_mesh.points[0]
    print tri_tuple(your_mesh, 2000)
    print len(your_mesh) # 4000
    print len(your_mesh.normals) # 4000

def all_pts_from_mesh(pMesh):
    """ Return all mesh points as a single list, even though the list is guaranteed to have many redundant points """
    
    pointsOnly = []
    
    for vList in [pMesh.v0 , pMesh.v1 , pMesh.v2]: # for each collection of points
        for point in vList: # for each point in the collection, check if the point is equal to any in the list of unique points
            pointsOnly.append(point) # The point is unique, add it to the unique list
    
    print "Collected",len(pointsOnly),"points."
    return pointsOnly

def all_pts_from_stl(STLpath):
    """ Return all STL points as a single list, even though the list is guaranteed to have many redundant points """
    if os.path.isfile(STLpath): # If a file exists at the location specified
        print "Found file at", STLpath
        STLdir, STLfile = os.path.split(STLpath)
        # Turn the closed hand STL into a point cloud only for quick collision checking
        hand_mesh = mesh.Mesh.from_file(STLpath)
        pointsOnly = all_pts_from_mesh(hand_mesh)
        print "Collected",len(pointsOnly),"points."
    else: # else warn the user a bad location provided
        print "uniq_pts_from_stl: No file with this name!"
        
    return pointsOnly

# FIXME: THIS WILL BE THOUSANDS OF TIMES FASTER IF YOU HASH THE COORDS, SEE URL BELOW
def uniq_pts_from_stl(STLpath, pklToFile = False):
    """ Return a set of unique points extracted from an STL file and store them as a list, write to file if 'pklToFile' """
    if os.path.isfile(STLpath):
        print "Found file at", STLpath
        STLdir, STLfile = os.path.split(STLpath)
        # Turn the closed hand STL into a point cloud only for quick collision checking
        handSTLm = STLpath #'simple-hand-closed_m.stl'
        hand_mesh = mesh.Mesh.from_file(handSTLm)
        print "uniq_pts_from_stl: STL model loaded",handSTLm
        # we cannot guarantee that v0 vectors of neighboring facets are not shared. Therefore, we have to iterate through all three
        #arrays of points, collecting the non-repeats into an array
        pointsOnly = []
        # URL, Comparison of uniqueifiers if you need this done faster: http://www.peterbe.com/plog/uniqifiers-benchmark
        for vList in [hand_mesh.v0 , hand_mesh.v1 , hand_mesh.v2]: # for each collection of points
            for point in vList: # for each point in the collection, check if the point is equal to any in the list of unique points
                if not eq_in_list(point, pointsOnly, eq_vec): # This is safe to do b/c iteration ends before the list is modified
                    pointsOnly.append(point) # The point is unique, add it to the unique list
        
        print "Collected",len(pointsOnly),"unique points."
    else:
        print "uniq_pts_from_stl: No file with this name!"
    
    if pklToFile: # Set to true to save the hand points
        pklPath = 'output/' + STLfile[:-4] + '_uniq-pts.pkl' # Give the file the same name, but with a PKL extension
        f = open( pklPath , 'wb') # open a file for binary writing to receive pickled data
        cPickle.dump(pointsOnly, f) # changed: pickle.dump --> cPickle.dump
        f.close()
        print "STL was cPickled to as a list of R3 points to file:", pklPath
        
    return pointsOnly

def load_pkl_struct(pklFilePath):
    """ Load a pickled object and return it, return None if error """
    fileLoaded = False
    rtnStruct = None
    try:
        f = open( pklFilePath , 'rb')
        fileLoaded = True
    except Exception as err:
        print "load_pkl_struct: Could not open file,",pklFilePath,",",err
    if fileLoaded:
        try:
            rtnStruct = cPickle.load(f)
        except Exception as err:
            print "load_pkl_struct: Could not unpickle file,",pklFilePath,",",err
        f.close()
    return rtnStruct

def plot_clusters(your_mesh):
    """ Plot all points in all clusters of the mesh """
    clusters = your_mesh['clusters']
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    numColors = len(basicColors)
    
    i = 0
    for cluster in clusters:
        xs,ys,zs = split_to_components( cluster )
        ax.scatter(xs, ys, zs, c=basicColors[i%(numColors-1)], marker='o')
        i += 1
    
    plot_axes_3D_mpl(ax, scale = 0.05) 
    plt.gca().set_aspect('equal')
    plt.show()

def plot_clusters_with_tiling(your_mesh):
    """ Plot the cluster edges overlaid with a tiling of candidate grasp points for each cluster """
    clusterEdges = your_mesh['clusterEdges']
    trialGraspPoints = your_mesh['trialGraspPoints']
    
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    
    basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    numColors = len(basicColors)
    
    i = 0
    for edge in clusterEdges:
        if len(edge) > 0:
            xs,ys,zs = split_to_comp_cycle( edge )
            #ax2.scatter(xs, ys, zs, c='k', marker='o', s=7) # basicColors[i%(numColors-1)]
            ax2.plot(xs, ys, zs, c='grey') # basicColors[i%(numColors-1)]
        i += 1
    
    i = 0
    for face in trialGraspPoints:
        xs,ys,zs = split_to_components( face )
        ax2.scatter(xs, ys, zs, c=basicColors[i%(numColors-1)], marker='o', s=14)
        i += 1
    
    plt.show()
    
def plot_clusters_with_stability(your_mesh, divisor = 10):
    """ Plot a mesh with the COM projected onto each of the clusters """
    clusterEdges = your_mesh['clusterEdges']
    stabilityStructs = your_mesh['clusterStability']
    COM = your_mesh['volumeCentroid']
    
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    
    basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    numColors = len(basicColors)
    
    #i = 0
    if divisor > 1:
        offset = random.randrange(divisor) # random offset will select a different group of facets each run
    else:
        offset = 0
    
    for i, edge in enumerate(clusterEdges): # Plot edges as closed polygons
        if len(edge) > 0 and (i+offset) % divisor == 0:
            xs, ys, zs = split_to_comp_cycle( edge )
            ax2.plot(xs, ys, zs, c = basicColors[i%(numColors-1)])  
    
    #i = 0
    for i, face in enumerate(stabilityStructs):
        if face and (i+offset) % divisor == 0:
            xs, ys, zs = split_to_components( [face['projectedCOM']] )
            ax2.scatter(xs, ys, zs, c = basicColors[i%(numColors-1)], marker='x', s=140)
            xs, ys, zs = split_to_components( [face['projectedCOM'], COM] )
            ax2.plot(xs, ys, zs, c = basicColors[i%(numColors-1)])
        #i += 1
        
    xs, ys, zs = split_to_components( [COM] )
    ax2.scatter(xs, ys, zs, c = 'k', marker = '+', s = 140)
    
    plt.gca().set_aspect('equal')
    plt.show()

def plot_opposing_grasp_pnts(your_mesh, scale = 0.02, divisor = 20):
    """ Plot pairs of candidate grasp pairs as vectors on obj surface pointing away from each other, with dotted line connecting """
    clusterEdges = your_mesh['clusterEdges']
    approvedGraspPairs = your_mesh['approvedGraspPairs']
    trialGraspPoints = your_mesh['trialGraspPoints']
    
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    
    basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    numColors = len(basicColors)
    
    for i, edge in enumerate(clusterEdges):
        if len(edge) > 0:
            xs,ys,zs = split_to_comp_cycle( edge )
            #ax2.scatter(xs, ys, zs, c='k', marker='o', s=7) # basicColors[i%(numColors-1)]
            ax2.plot(xs, ys, zs, c='grey') # basicColors[i%(numColors-1)]
    
    for n, pair in enumerate(approvedGraspPairs):
        if n % divisor == 0:
            # Construct a unit vector between the origins of the two clusters
            originI = item_at_addr(trialGraspPoints, pair[0])
            originJ = item_at_addr(trialGraspPoints, pair[1])
            pointAwayI = np_unit_vec( np.subtract( originI , originJ ) )
            pointAwayJ = np_unit_vec( np.subtract( originJ , originI ) )
            pointI = np.add( originI , np.multiply( pointAwayI , scale ) )
            pointJ = np.add( originJ , np.multiply( pointAwayJ , scale ) )
            xs,ys,zs = split_to_components( (originI,pointI) )
            ax2.plot(xs, ys, zs, c=basicColors[n%(numColors-1)]) # 
            xs,ys,zs = split_to_components( (originJ,pointJ) )
            ax2.plot(xs, ys, zs, c=basicColors[n%(numColors-1)]) # 
            xs,ys,zs = split_to_components( (originI,originJ) )
            ax2.plot(xs, ys, zs, c=basicColors[n%(numColors-1)], ls='-.') # '--' ':'
            
    plt.gca().set_aspect('equal')    
    plt.show()

# = End STL =    
  
# = Point Cloud Helpers =
  
def bound_box_3D(R3PointsList):
    """ Given a 'R3PointsList', return a nested tuple of the 3D Bounding Box ( (xMin,xMax) , (yMin,yMax) , (zMin,zMax) ) """
    xMin =  infty
    xMax = -infty
    yMin =  infty
    yMax = -infty
    zMin =  infty
    zMax = -infty
    
    for point in R3PointsList:
        # x extent
        if point[0] < xMin:
            xMin = point[0]
        if point[0] > xMax:
            xMax = point[0]
        # y extent    
        if point[1] < yMin:
            yMin = point[1]
        if point[1] > yMax:
            yMax = point[1]
        # z extent
        if point[2] < zMin:
            zMin = point[2]
        if point[2] > zMax:
            zMax = point[2]
            
    return ( (xMin,xMax) , (yMin,yMax) , (zMin,zMax) )
    
def span_3D(bbox3D):
    """ Given a 'bbox3D' ( (xMin,xMax) , (yMin,yMax) , (zMin,zMax) ), Return the extent along each axis """
    return ( abs(bbox3D[0][1] - bbox3D[0][0]) , abs(bbox3D[1][1] - bbox3D[1][0]) , abs(bbox3D[2][1] - bbox3D[2][0]) )

def volume_centroid_of_points(R3PointsList, numSlices = 100):
    """ Return the centroid of a 3D object for which 'R3PointsList' forms a point cloud boundary, assuming uniform desnity """
    # numSlices: number of z slices to partition point cloud into
    
    # Establish the extent of the z-axis that the model spans, 'zMin' to 'zMax'
    zMin =  infty
    zMax = -infty
    for center in R3PointsList:
        if center[2] < zMin:
            zMin = center[2]
        if center[2] > zMax:
            zMax = center[2]

    # Init z slices of model
    zSpan = abs(zMax - zMin) # z extent that the model spans
    sliceThknss = zSpan * 1.0 / numSlices # the z thickness of each slice
    zSlices = [[] for i in range(numSlices)]
    sliceCount = [0 for i in range(numSlices)]

    # Flatten all the points found in each z-slize with 'sliceThknss' onto a plane
    loopCount = 0
    for center in R3PointsList:
        # Find the index of the slice that this point belongs to
        index = int( abs(center[2] - zMin) / sliceThknss ) # 'int' rounds down to the next integer
        if index >= numSlices:
            index = numSlices - 1
        #index = abs(center[2] - zMin) / sliceThknss
        #print index,center[:-1],'\t',
        zSlices[index].append( center[:-1] ) # store only the x,y coordinates of the point in the slice
        #zSlices[index].append( index ) # store only the x,y coordinates of the point in the slice
        sliceCount[index] += 1
        loopCount += 1
 
    # form the convex hull of each slice
    sliceBounds = []
    #index = 0
    for index, zSlc in enumerate(zSlices):
        sliceBounds.append([])
        try:
            sliceHull = sci_spatial.ConvexHull( zSlc ) # zSlices[index] )#, qhull_options='Qm' ) # Not 100% certain 'Qm' is needed?
            sliceBounds[-1] = bounding_points_from_hull(sliceHull)
        except Exception as err:
            dbgLog(1, "Encountered" , type(err).__name__ , "on index", index , "with args:", err.args,"with full text:",str(err) )
        #index += 1

    # Compute the cross sectional area, compute the center of the slice
    slcCentroids = []
    slcTotalArea = 0
    for bndDex, bound in enumerate(sliceBounds):
        if len(bound): # If our convex hull was succesfull
            A = 0
            triCentroids = list() # []
            totArea = 0
            for pntDex in range( 2 , len(bound) ):
                # http://www.mathopenref.com/coordtrianglearea.html
                # Assume that we are dealing with convex polygons (hulls), otherwise use shoelace algorithm
                Ax = bound[pntDex][0]
                Bx = bound[pntDex-1][0]
                Cx = bound[0][0]
                Ay = bound[pntDex][1]
                By = bound[pntDex-1][1]
                Cy = bound[0][1]
                A = 0.5 * abs( Ax*(By-Cy) + Bx*(Cy-Ay) + Cx*(Ay-By) )
                totArea += A
                triCentroids.append( ( A , vector_mean( bound[pntDex] , bound[pntDex-1] , bound[0] ) ) )
            slcTotalArea += totArea
            flatCentroid = centroid_discrete_masses(triCentroids,totArea)
            slcCentroids.append( ( totArea , [ flatCentroid[0] , flatCentroid[1] , zMin + sliceThknss/2 + bndDex * sliceThknss ] ) )
        else: # Else produce a slice of zero area, and assume there wille enough good slices to produce a volume centroid
            slcCentroids.append( ( 0 , [ 0 , 0 , 0 ] ) )
        #print "Calculated an area of",totArea,"for this slice"
        
    return centroid_discrete_masses(slcCentroids,slcTotalArea)

# = End Point Cloud =
  
# = Batch Logging =  
dbgStr = '' # A place to put debug and status information for this session, cleared at start of session when file is run

DEBUGLEVEL = 1

def set_dbg_lvl(level):
    """ Set the 'DEBUGLEVEL' for this session, must be an integer """
    global DEBUGLEVEL # Apparently you cannot access global variables from another module?
    DEBUGLEVEL = int(level)
    
def get_dbg_lvl(level):
    """ Return the 'DEBUGLEVEL' for this session """
    return DEBUGLEVEL
    
def dbgLog(lvl, *dbgMsg):
    """ Write status info to the debug log and print if desired """
    global dbgStr
    if lvl == DEBUGLEVEL:
        temp = ''
        for msg in dbgMsg:
            temp += str(msg) + ' ' # Auto-insert spaces between args just like 'print <OUTPUT>,'
        dbgStr += temp + endl 
        print temp

def dbg_data_exists():
    """ Return True if debug data exists, otherwise false """
    return True if dbgStr else False
    
def dbg_contents():
    """ Return the contents of the debug string """
    return dbgStr
    
def dbgClr():
    """ Clear status info for this session """
    global dbgStr
    dbgStr = '' # Set debug string to an empty string
# = End Logging =
  
# == End Helpers ==
  
# == Benchmark Stopwatch ==
class Stopwatch(object):
    """ Singleton object for benchmarking """
    strtTime = 0
    stopTime = 0
    @staticmethod
    def start():
        Stopwatch.strtTime = timer()
    @staticmethod
    def stop():
        Stopwatch.stopTime = timer()
    @staticmethod
    def elapsed():
        return Stopwatch.stopTime - Stopwatch.strtTime
# == End Stopwatch ==
        
# == Abandoned Code ==
        
#def add_module_to_path( modPath , modName ):
#    """ Add a module to the path and return a reference to it """    
#    # Adapted from work by Fredrik Lundh: http://effbot.org/zone/import-string.htm    
#
#    #directory, module_name = os.path.split(modPath)
#    #module_name = os.path.splitext(module_name)[0]
#    moduleRef = None
#
#    path = list(sys.path)
#    sys.path.insert(0, modPath)
#    try:
#        moduleRef = __import__( modName )
#    finally: # A finally clause is always executed before leaving the try statement, whether an exception has occurred or not.
#        sys.path[:] = path # restore
#        
#    return moduleRef
        
# == End Abandoned ==