# -*- coding: utf-8 -*-
"""
mesh_test.py
James Watson, 2016 February
Analyze grip location on an STL file.

~~ Dependencies ~~
numpy :     To support numpy-stl
numpy-stl : To process STL files
scipy :     To calculate nearest neighbors in point clouds
"""

"""
== LOG ==
2016-03-08: Clusters are populated with trial grasp points!
2016-02-20: * Flattened clusters yield clean convex hulls.
2016-02-18: Added an angle condition to the facet acceptance criteria
2016-02-14: Tested basic point operations in numpy-stl

== TODO ==
* Generalize the code below to accept any model
* give weights to both the facet center distance and the normal separation angle so that they are not straigh pass-fail 
  criteria
  
== NOTES ==
* Use the magic command '%matplotlib' to tell IPython to draw plots in separate windows. (Interactive 3D plots!)
  - Is there a way to set this in the script itself?
  
== IDEAS ==
* Throw away STL facets to make less work on the grasp planner
"""
# ~~ Imports ~~
# ~ Standard ~
import numpy as np
import scipy.spatial as sci_spatial 
import random
from math import cos, radians #sqrt, ceil, sin, , tan, atan2, asin, acos, atan, pi, degrees, , log10
from os import linesep
# ~ Special ~
from stl import mesh

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = linesep

# ~~ Helper Functions ~~

def eq(op1, op2):
    """ Return true if op1 and op2 are ''close enough'' """
    return abs(op1 - op2) <= EPSILON
 
def sep(title = ""):
    """ Print a separating title card for printing """
    LINELEN = 8
    LINE = '=' * LINELEN
    SPACE = ' ' if title else ''
    print LINE + SPACE + title + SPACE + LINE


# Load the STL files 
your_mesh = mesh.Mesh.from_file('spam_simplified.stl')

print "Found", len(your_mesh.normals), "normals" # the normal vector of each facet # Found 4000 normals
print "Found", len(your_mesh.v0), "vectors in V0" # 1st vertex of triangular facet # Found 4000 vectors in V0
print "Found", len(your_mesh.v1), "vectors in V1" # 2st vertex of triangular facet # Found 4000 vectors in V1
print "Found", len(your_mesh.v2), "vectors in V2" # 3st vertex of triangular facet # Found 4000 vectors in V2
print "Found", len(your_mesh.points), "points" #points (concatenation of v0, v1 and v2 in triplets) # Found 4000 points

print your_mesh.points[0]

def len_mesh(mesh):
    return len(mesh.normals)

# == Structure of an STL file ==
# URL: https://en.wikipedia.org/wiki/STL_%28file_format%29#Binary_STL

#Each triangle is described by twelve 32-bit floating-point numbers: three for the normal and then three for the X/Y/Z 
#coordinate of each vertex. After these follows a 2-byte ("short") unsigned integer that is the "attribute byte count" – 
#in the standard format, this should be zero because most software does not understand anything else.

# == End Structure ==

def tri_tuple(mesh, index):
    """ Return a tuple of (normal, Vertex0, Vertex1, Vertex2) """
    if index <= len_mesh(mesh) - 1:
        return (mesh.normals[index], your_mesh.v0[index], your_mesh.v1[index], your_mesh.v2[index])
    else:
        raise IndexError("tri_tuple: Index " + str(index) + " was out of bounds! Size " + str(len_mesh(mesh)) if mesh else "UNKNOWN")
        
print tri_tuple(your_mesh, 2000)
#(array([  6.74260991e-06,  -8.89091643e-06,  -2.65523431e-06], dtype=float32), # Normal
# array([ 0.02187921, -0.00653373,  0.00653129], dtype=float32), # v0
# array([ 0.02146451, -0.00756007,  0.00891486], dtype=float32), # v1
# array([ 0.01810098, -0.00948165,  0.00680794], dtype=float32)) # v2

print len(your_mesh) # 4000
print len(your_mesh.normals) # 4000

def vector_mean(*args): # This does the same thing as np.mean?
    rtnVec = []
    for index in xrange(len(args[0])): # assumes that all args are vectors of the same length
        rtnVec.append( 0 )
        for arg in args:
            rtnVec[index] += arg[index]
        rtnVec[index] /= len(args[0])
    return rtnVec

def tri_centers(mesh):
    rtnPnts = []
    for i in xrange(len(mesh)):
        rtnPnts.append( vector_mean( mesh.v0[i] , mesh.v1[i] , mesh.v2[i]) )
        #rtnPnts.append( np.mean( mesh.v0[i] , mesh.v1[i] , mesh.v2[i]) )
    return rtnPnts

sep("For some reason query returns the node itself as the closest node")
triCenters = tri_centers(your_mesh)
print triCenters[4]
#print your_mesh.v0[1]
nearNeighbors = sci_spatial.cKDTree(triCenters, leafsize=100)
print nearNeighbors.query(triCenters[4], k=3, distance_upper_bound=6)
print triCenters[4], "distance", sci_spatial.distance.euclidean( triCenters[4] , triCenters[4] )
print triCenters[5], "distance", sci_spatial.distance.euclidean( triCenters[4] , triCenters[5] )
print triCenters[9], "distance", sci_spatial.distance.euclidean( triCenters[4] , triCenters[9] )
#print your_mesh.v0[52]
print len(triCenters) # 4000

sep("Try again on v0")
nearNeighbors = sci_spatial.cKDTree(your_mesh.v0, leafsize=100)
print nearNeighbors.query(your_mesh.v0[4], k=3, distance_upper_bound=6)
print your_mesh.v0[52], "distance", sci_spatial.distance.euclidean( your_mesh.v0[4] , your_mesh.v0[52] )
print your_mesh.v0[9], "distance", sci_spatial.distance.euclidean(  your_mesh.v0[4] , your_mesh.v0[9]  )
print your_mesh.v0[60], "distance", sci_spatial.distance.euclidean( your_mesh.v0[4] , your_mesh.v0[60] )
#[-0.03231296 -0.02681846  0.01029602] distance 0.0
#[-0.03231296 -0.02681846  0.01029602] distance 0.0
#[-0.03231296 -0.02681846  0.01029602] distance 0.0
# This shows that the same point is the v0 for multiple facets, for now operate on the average centers to avoid repeats

sep( "Investigate length of the normals" )
total = 0
for normal in your_mesh.normals:
    total += np.linalg.norm(normal)
print "Average normal length is", total/len(your_mesh) 
#     "Average normal length is 1.46460835567e-05" # NOT a unit vector, in other words


sep( "Attempt clustering of the object surface" )
# Grasp Planning for Parallel Grippers with Flexibility on its Grasping Surface, Kensuke Harada

# This is slightly different than the clustering method presented in the above. Instead of picking pairs of facets, I 
#just ask the k-D tree for the N nearest neighbors to the node under scrutiny. These facets are added to the cluster in 
#order until one of them fails the height test. The facet that failed will become the nucleus of a new cluster

# == Helper Functions ==
# = Geometry Helpers =
def np_vec_proj(a,b):
    """ a projected onto b, a scalar length, using numpy """
    return np.dot(a,b) / np.linalg.norm(b)
    
def np_unit_vec(vec):
    """ Return a unit vector in the direction of 'vec', using numpy """
    return np.divide( vec , np.linalg.norm(vec) )
    
def split_to_components( vecList ):
    """ Separate a list of R3 vectors into three lists of components """ # because matplotlib likes it that way
    plotXs = []
    plotYs = []
    plotZs = []
    for vec in vecList:
        plotXs.append( vec[0] )
        plotYs.append( vec[1] )
        plotZs.append( vec[2] )
    return plotXs, plotYs, plotZs
    
def split_to_comp_cycle( vecList ):
    plotXs, plotYs, plotZs = split_to_components( vecList )
    plotXs.append( plotXs[0] )
    plotYs.append( plotYs[0] )
    plotZs.append( plotZs[0] )
    return plotXs, plotYs, plotZs
    
def split_to_2D( vecList ): 
    """ Separate a list of R2 vectors into two lists of components """ # because matplotlib likes it that way
    plotXs = []
    plotYs = []
    for vec in vecList:
        plotXs.append( vec[0] )
        plotYs.append( vec[1] )
    return plotXs, plotYs
    
def split_2d_on_XY_Zeq0( vecList ): 
    """ Separate a list of R2 vectors into three lists of components on the X-Y plane, Z = 0 """ 
    plotXs = []
    plotYs = []
    plotZs = []
    for vec in vecList:
        plotXs.append( vec[0] )
        plotYs.append( vec[1] )
        plotZs.append(      0 )
    return plotXs, plotYs, plotZs
    
def bounding_points_from_hull(hull):
    """ Given a scipy.spatial.ConvexHull, return a list of the (supposedly) bounding points """
    # NOTE: Somehow the ConvexHull was not so convex?
    pntList = []
    # ConvexHull.vertices contains the indices of ConvexHull.points that are the bounding poly
    for index in hull.vertices: 
        pntList.append( hull.points[index] )
    return pntList
    
def cosd(angleDeg):
    """ Return the cosine of the angle, given in degrees """
    return cos( radians( angleDeg ) )
# = End Geo =
# = Plotting Helpers =
def plot_axes_3D_mpl(axes, scale = 1): 
    """ Display the coordinate axes in standard XYZ-RGB on a matplotlib 3D projection, each vector 'scale' in length """
    # NOTE: This function assumes that 'axes' has already been set up as a 3D projection subplot
    # URL, Show Origin: http://stackoverflow.com/a/11541628/893511
    # ax.plot( Xs    , Ys    , Zs    , c=COLORNAME )
    axes.plot( [0,scale] , [0,0    ] , [0,0    ] , c='r')
    axes.plot( [0,0    ] , [0,scale] , [0,0    ] , c='g')
    axes.plot( [0,0    ] , [0,0    ] , [0,scale] , c='b')
    
def plot_bases_3D_mpl(plotAX, origin, bX, bY, bZ, scale): 
    """ Display the supplied axes in standard XYZ-RGB on a matplotlib 3D projection, each vector 'scale' in length """
    # NOTE: This function assumes that 'axes' has already been set up as a 3D projection subplot
    # NOTE: This function does not perform any checks whatsoever on the orthogonality or length of bX, bY, bZ
    # URL, Show Origin: http://stackoverflow.com/a/11541628/893511
    # ax.plot( Xs    , Ys    , Zs    , c=COLORNAME )
    Xs, Ys, Zs = split_to_components( [ origin , np.add( np.multiply( bX , scale ) , origin) ] )
    plotAX.plot( Xs, Ys, Zs , c='r')
    Xs, Ys, Zs = split_to_components( [ origin , np.add( np.multiply( bY , scale ) , origin) ] )
    plotAX.plot( Xs, Ys, Zs , c='g')
    Xs, Ys, Zs = split_to_components( [ origin , np.add( np.multiply( bZ , scale ) , origin) ] )
    plotAX.plot( Xs, Ys, Zs , c='b')
# = End Plotting =
# == End Helpers ==

# = Clustering Init =
totalNodes = len(your_mesh) # get the number of triangles in the mesh
#curNode = random.randrange(totalNodes) # retrieve index of a random node 
curNode = 1660 # FIXME : return to random start nucleus after testing is satisfactory
print "DEBUG: Chose the node at index", curNode, "of model points"
hLimit = 0.01 #0.001 # The height limit for clustering # NOT EVEN SURE WHAT SCALE?
pLimit = cosd( 30 )  # projection limit, any facet that has a unit normal more than 7 degrees away from the nuclues unit 
#                      normal will be rejected
usedFacets = set([]) # set of facet indices that have become part of a cluster
unusedFacets = set(range(totalNodes)) # set of facet indices that have NOT become part of a cluster
clusterLimit = 1000 # tha maximum number of facets that will be considered, per cluster
triCenters = tri_centers(your_mesh) # get the coords of every facet center in the model
nearNeighbors = sci_spatial.cKDTree(triCenters, leafsize=100) # generate k-D tree of nearest neighbors
clusters = [] # list of clusters, new pseudo-faces for calc of candidate grasp
clusterNorms = [] # the unit normal vectors for all clusters created
nuChosen = True # flag is true if a nucleus facet has been chosen for the next cluster
# = End Init =

while len(unusedFacets) > 0: # while there are unclustered points in the model, create a new cluster

    #print "LOOP DEBUG:", # DEBUG
    
    lastNode = curNode # store the index of the last nucleus in the model
    
    # per new cluster
    curNeighbors = nearNeighbors.query(triCenters[curNode], k=clusterLimit, distance_upper_bound=6) # get the 'clusterLimit' nearest neighbors to current node
    addCount = notCount = 0 # init count of clustered nodes
    clusters.append( [ your_mesh.v0[curNode] , your_mesh.v1[curNode] , your_mesh.v2[curNode] ] ) # add points of the nucleus to the cluster, guaranteed one
    unusedFacets.remove(curNode) # the nucleus facet is removed, it is part of a cluster even if it is the only member
    #print "Began a new cluster at",curNode,", removed.", # DEBUG
    curNorm = np_unit_vec( your_mesh.normals[curNode] ) # get the normal of the nucleus facet for this cluster, convert to unit, to define the plane
    clusterNorms.append( curNorm ) # store the unit vector for this cluster
    planePnt = triCenters[curNode] # get a reference point on the nucleus facet so that we can calc per distance
    
    lastLoopAdd = 0
    lastLoopSkip = 0 # DEBUG: How many we would have skipped were we to keep searching after first failure
    
    clusterEnd = False # fresh cluster, perform all aggregating activities
    
    #print "DEBUG: Begin a new cluster at", curNode
    
    for facet in xrange(1, clusterLimit): # per neighbor facet in the list of nearest neighbors
        #print facet, "\t",
        facDex = curNeighbors[1][facet] # index of this neighbor in the list of mesh points, where we are in the mesh
        if not clusterEnd: # only perform calcs if 
            w = np.subtract( triCenters[facDex] , planePnt ) # separating vector between trial facet and the nucleus reference point
            curH = abs(np_vec_proj(w,curNorm)) # calculate the perpendicular separation distance between the facet center and the nucleus plane
            facetNorm = np_unit_vec( your_mesh.normals[facDex] ) # get the unit normal of the facet under scrutiny
            curP = np_vec_proj(facetNorm,curNorm)
        # TODO : consider updating the nucleus plane to some average of normals/points after every N facets have been processed.
        #        This way a nucleus that is "off the true local plane" does not cause the cluster to skip over a naturally flat 
        #        surface. [WOULD AN AVERAGE OF CENTERS BE ON THE NEW PLANE? TEST!]
        #print curH, "\t",
        if not clusterEnd and curH < hLimit and curP > pLimit: # no prev test failed, 
        #                                                        perp distance between neighbor center and nucleus is within limit, 
        #                                                        angle between nucleus and facet is below limit, add to cluster
            #print "DEBUG: Add the facet"
            if facDex in unusedFacets: # if the current facet has not been assigned to a cluster yet
                #print "Added",facDex,".", # DEBUG
                addCount += 1 
                lastLoopAdd = facet
                unusedFacets.remove(facDex) # mark this facet as clustered
                #print "DEBUG: Removed facet at index", facDex, "in mesh."
                #threePoints =[] # used to structure vertices of a facet as one list
                for point in [ your_mesh.v0 , your_mesh.v1 , your_mesh.v2 ]: # for each vertex of the facet
                    w = np.subtract( point[facDex] , planePnt ) # generate a separation vector with respect to the nucleus reference
                    curH = np_vec_proj(w,curNorm) # calc the perp separation from the vertex to the cluster plane
                    clusters[-1].append( np.subtract( point[facDex] , np.multiply( curNorm , curH ) ) ) # append point projected onto nucleus plane
            else:
                #print "Skipped",facDex,"for adding.", # DEBUG
                pass
            #else: We encountered a used facet but have not yet failed a distance test, continue aggregating facets
        else: # else neighbor is too far from nucleus plane, do not add to cluster, break
            #print "DEBUG: Do not add facet"
            clusterEnd = True # switch the loop to a mode in which it searches for the next cluster nucleus
            notCount += 1
            lastLoopSkip = facet
            if facDex in unusedFacets:
                curNode = facDex # get the mesh index of the first point that failed to be clustered, this will be the nucleus of the next iteration
                #curNode = curNeighbors[1][facet] # get the mesh index of the first point that failed to be clustered, this will be the nucleus of the next iteration
                #print "Located new nucleus at",facDex,".\n" # DEBUG
                break # stop building this cluster on the first addition failure
            else: #continue to the next closest node.  
                #print "Skipped",facDex,"for new nucleus.", # DEBUG
                # We have not removed used nodes from the neighborhood calc above, so facets at cluster boundaries could potientially be iterated over many times
                # TODO : SHOULD I EVEN SEARCH NEIGHBORS AT THIS POINT?  WHAT DO I HAVE TO LOSE FROM NUCLEATING A CLUSTER AT A RANDOM LOCATION?
                pass
                
            # If the above 'break' statement is uncommented, then less facets are added to the cluster. This is because there
            #are some facets that are outside the cluster criteria that have farther neighbors that are within the cluster 
            #criteria. Continuing to search after the criteria fails results in a wider cluster, but does not leave an automatic
            #way to include the skipped nodes in the current cluster, nor to take any interior cluster into account
    
    # If we are here and 'curNode' has not changed, it means we exhausted all the options in the nearest neighbors for a nucleus, we have to pick from the 
    #remaining unused nodes throughout the model. Hopefully they are not spread out all over?
    if curNode == lastNode and len(unusedFacets) > 0:
        curNode = unusedFacets.pop() # return an arbitrary facet # URL: https://docs.python.org/2/library/stdtypes.html#set.pop
        unusedFacets.add(curNode) # Python sets are not indexable, and pop is destructive, so we have to add this index back on for the sake of the next loop, 
        #                           which will remove it
            
# TODO: Seed clusters with candidate grasps somewhere in the above code. Likely doing it outside will repeat work since we have already generated
#       a bunch of geometric info?

#print "Added", addCount, "facets. Last adding loop:", lastLoopAdd # DEBUG
#print "Skipped", notCount, "facets. Last skipping loop:", lastLoopSkip # DEBUG
print np.linalg.norm(triCenters[0]) # use numpy to calc vector magnitude
print np.subtract(triCenters[0] , triCenters[40]) # use numpy to subtract vectors
#print clusters[0]

# Now that a collection of clusters spanning the entire model has been obtained, plot them all


# == Plot Clusters ==
from mpl_toolkits.mplot3d import Axes3D # required on some distributions to use projection='3d', ignore "unused" warning
import matplotlib.pyplot as plt
plt.close('all') # clear any figures we may have created before
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
numColors = len(basicColors)

i = 0
for cluster in clusters:
    xs,ys,zs = split_to_components( cluster )
    ax.scatter(xs, ys, zs, c=basicColors[i%(numColors-1)], marker='o')
    i += 1

plt.show()
# == End Plot ==

# make a proper, planar convex hull of each of the clusters, no fuzzy edges this time, please
i = 0 # keep track of the cluster index
flatClusters = [] # a collection of flattened clusters for computing convex hulls #  Each of these have a len(*) == len(clusters)              
flatBases = [] # a list of origins and basis vectors used for analyzing the clusters #      <'         /
flatBoxes = [] # a list of bounding boxes for each of the clusters, expressed in the cluster basis # <'
for cluster in clusters:
    clusLen = len(cluster)
    flatClusters.append( [] ) # append an empty list to be filled with flattened vectors
    # Create two basis vectors for this cluster
    # pick two random points in the cluster to be a basis vector, choose from two halves so that they are not the same
    xHead = cluster[ random.randrange(clusLen/2) ] # head of the x basis vector                                                             
    xTail = cluster[ random.randrange(clusLen/2,clusLen) ] # tail of the x basis vector, use as the origin for coordinate transformation    
    pOffset = np.subtract( xHead  , xTail ) # xTail , xHead
    xVec = np_unit_vec( pOffset ) # construct x unit basis vector based on two random points in our collection
    yVec = np_unit_vec( np.cross( clusterNorms[i], xVec ) ) # obtain a vector orthogonal both to the normal and the normal
    transfOffset = [ np_vec_proj(pOffset, xVec) , np_vec_proj(pOffset, yVec) ]
   #zVec = the normal vector for this cluster
    cXmax = -infty
    cXmin =  infty
    cYmax = -infty
    cYmin =  infty
    # Place all the points in the cluster in an R2 plane and find the bounding box
    for point in cluster:
        transformedX = np_vec_proj(np.subtract(point , xTail), xVec) 
        transformedY = np_vec_proj(np.subtract(point , xTail), yVec) 
        flatClusters[-1].append( [ transformedX , transformedY ] )
        cXmax = transformedX if transformedX > cXmax else cXmax
        cXmin = transformedX if transformedX < cXmin else cXmin
        cYmax = transformedY if transformedY > cYmax else cYmax
        cYmin = transformedY if transformedY < cYmin else cYmin
    flatBases.append( {'origin': xTail, 
                      'orgXDir': xHead,         
                         'xVec': xVec,          
                         'yVec': yVec,          
                         'zVec': clusterNorms[i],
                      'xOffset': pOffset } )    
    flatBoxes.append( ( ( cXmin , cYmin ) , ( cXmax , cYmax ) ) ) # ( ("lower-left" coords) , ("upper-right" coords) ) 
    i += 1 # increment index


i = 0
errCount = 0
clusterEdges = [] # Collections of 3D points that represent the cluster boundaries
edges2D = [] # Collections of 2D points that represent the convex hull of a cluster projected onto a plane based on the constructed basis vectors
for fCluster in flatClusters:
    #if i == 14:
    #    break
    clusterEdges.append( [] ) # init a new cluster edge, NOTE: Note if there is an error below this list will be EMPTY
    edges2D.append( [] ) # init a new 2D edge, NOTE: Note if there is an error below this list will be EMPTY
    try: # attempt to form a convex hull from the flattened cluster, then select those cluster members at the edges and add them to 'clusterEdges'
        flatHull = sci_spatial.ConvexHull( flatClusters[i] ) # generate a convex hull of the flattened cluster
        for index in flatHull.vertices:
            clusterEdges[-1].append( clusters[i][index] ) # Add a point to the collection that represents the cluster boundary in 3D space
            edges2D[-1].append( flatClusters[i][index] ) #  Add a point to the collection that represents the cluster boundary in 2D space
    except Exception as ex: # URL, generic excpetions: http://stackoverflow.com/a/9824050
        print "Encountered" , type(ex).__name__ , "on index", i , "with args:", ex.args
        errCount += 1
    i += 1

print "Encountered", errCount, "errors while flattening clusters. There are ", len(clusters), "clusters.", \
       "{0:.2f}".format(errCount * 100.0 / len(clusters)) , "% error rate"
# 15-23 errors with the same data.  It is not the same each run!

# For now, try to continue with the failed clusters.  Do I need to use another convex hull implementation?
# Try to tile each of the clusters with N potential grasp points, rowsN*colsN

def elemw(i, iterable):
    """ Return the 'i'th index of 'iterable', wrapping to index 0 at all integer multiples of 'len(iterable)' """
    return iterable[ i % ( len(iterable) ) ]

def winding_num(point , polygon):
    """ Find the winding number of a point with respect to a polygon """
    # NOTE: Function assumes that the points of 'polygon' are ordered. Algorithm does not work if they are not
    # This algorithm is translation invariant, and can handle convex, nonconvex, and polygons with crossing sides.
    # This algorithm does NOT handle the case when the point lies ON a polygon side. For this problem it is assumed that
    #there are enough trial points to ignore this case
    # This works by shifting the point to the origin (preserving the relative position of the polygon points), and 
    #tracking how many times the positive x-axis is crossed
    w = 0
    v_i = []
    x = lambda index: elemw( index , v_i )[0] # We need wrapping indices here because the polygon is a cycle
    y = lambda index: elemw( index , v_i )[1]
    for vertex in polygon: # Shift the point to the origin, preserving its relative position to polygon points
        v_i.append( np.subtract( vertex , point ) )
    for i in range(len(v_i)): # for each of the transformed polygon points, consider segment v_i[i]-->v_i[i+1]
        if y(i) * y(i + 1) < 0: # if the segment crosses the x-axis
            r = x(i) + ( y(i) * ( x(i + 1) - x(i) ) ) / ( y(i) - y(i + 1) ) # location of x-axis crossing
            if r > 0: # positive x-crossing
                if y(i) < 0: 
                    w += 1 # CCW encirclement
                else:
                    w -= 1 #  CW encirclement
        # If one of the polygon points lies on the x-axis, we must look at the segments before and after to determine encirclement
        elif ( eq( y(i) , 0 ) ) and ( x(i) > 0 ): # v_i[i] is on the positive x-axis, leaving possible crossing
            if y(i) > 0:
                w += 0.5 # possible CCW encirclement or switchback from a failed CW crossing
            else:
                w -= 0.5 # possible CW encirclement or switchback from a failed CCW crossing
        elif ( eq( y(i + 1) , 0 ) ) and ( x(i + 1) > 0 ): # v_i[i+1] is on the positive x-axis, approaching possible crossing
            if y(i) < 0:
                w += 0.5 # possible CCW encirclement pending
            else:
                w -= 0.5 # possible  CW encirclement pending
    return w

def point_in_poly_w(point , polygon):
    """ Return True if the 'polygon' contains the 'point', otherwise return False, based on the winding number """
    return not eq( winding_num(point , polygon) , 0 ) # The winding number gives the number of times a polygon encircles a point

rowsColsN = 4 # tile each cluster bounding box with 'rowsColsN ** 2' points, only the points that lie with the full will be kept
rowsN = 2 + rowsColsN # number of rows of test points to generate
colsN = 2 + rowsColsN # number of cols of test points to generate

index = 0 # cluster index
trialGraspPoints = []
for poly in edges2D:
    trialGraspPoints.append( [] ) # Add a list item even if the hull fails in order to maintain index parity
    if len(poly) > 0: # if we were actually able to construct a hull
        #         flatBoxes[index] : ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
        rowSpan = ( flatBoxes[index][1][1] - flatBoxes[index][0][1] ) / (rowsN - 1) # height of a row
        colSpan = ( flatBoxes[index][1][0] - flatBoxes[index][0][0] ) / (colsN - 1) # width of a col
        # Create a grid of test points that span the bounding box, and keep the ones within the convex hull as potential grasp points
        for i in range(1 , rowsN - 1): # for every row of test points         
            for j in range(1 , colsN - 1): # for every column of test points  
                testPoint = ( flatBoxes[index][0][0] + colSpan * j,
                              flatBoxes[index][0][1] + rowSpan * i )
                if point_in_poly_w( testPoint , poly ): # the point is inside the flattened hull, create the trial grasp in 3D space on the model
                    # flatBases[i] = {'origin': xTail, 'xVec': xVec , 'yVec': yVec, 'zVec': clusterNorms[i] }
                    trialGraspPoints[-1].append( np.add( flatBases[index]['origin'],                                            
                                                         np.add( np.multiply( flatBases[index]['xVec'] , testPoint[0]  ) ,      
                                                                 np.multiply( flatBases[index]['yVec'] , testPoint[1]  ) ) ) )    
    index += 1 # increment cluster index
            

fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')

i = 0
for edge in clusterEdges:
    if len(edge) > 0:
        xs,ys,zs = split_to_comp_cycle( edge )
        #ax2.scatter(xs, ys, zs, c='k', marker='o', s=7) # basicColors[i%(numColors-1)]
        ax2.plot(xs, ys, zs, c='k') # basicColors[i%(numColors-1)]
    i += 1

i = 0
for face in trialGraspPoints:
    xs,ys,zs = split_to_components( face )
    ax2.scatter(xs, ys, zs, c=basicColors[i%(numColors-1)], marker='o', s=14)
    i += 1

plt.show()


# %matplotlib


"""  What is wrong with these clusters?
Encountered QhullError on index 11 with args: ('Qhull error',) # how informative. Thanks Qhull!
Encountered QhullError on index 31 with args: ('Qhull error',)
Encountered QhullError on index 38 with args: ('Qhull error',)
Encountered QhullError on index 48 with args: ('Qhull error',)
Encountered QhullError on index 50 with args: ('Qhull error',)
Encountered QhullError on index 57 with args: ('Qhull error',)
Encountered QhullError on index 63 with args: ('Qhull error',)
Encountered QhullError on index 72 with args: ('Qhull error',)
Encountered QhullError on index 84 with args: ('Qhull error',)
Encountered QhullError on index 90 with args: ('Qhull error',)
Encountered QhullError on index 103 with args: ('Qhull error',)
Encountered QhullError on index 117 with args: ('Qhull error',)
Encountered QhullError on index 126 with args: ('Qhull error',)
Encountered QhullError on index 140 with args: ('Qhull error',)
Encountered QhullError on index 145 with args: ('Qhull error',)
Encountered QhullError on index 149 with args: ('Qhull error',)
Encountered QhullError on index 150 with args: ('Qhull error',)
Encountered QhullError on index 157 with args: ('Qhull error',)
Encountered QhullError on index 185 with args: ('Qhull error',)
Encountered QhullError on index 210 with args: ('Qhull error',)
Encountered QhullError on index 211 with args: ('Qhull error',)
Encountered QhullError on index 215 with args: ('Qhull error',)
Encountered QhullError on index 219 with args: ('Qhull error',)
Encountered QhullError on index 220 with args: ('Qhull error',)
Encountered QhullError on index 222 with args: ('Qhull error',)
Encountered QhullError on index 225 with args: ('Qhull error',)
Encountered QhullError on index 227 with args: ('Qhull error',)
Encountered QhullError on index 249 with args: ('Qhull error',)
Encountered QhullError on index 253 with args: ('Qhull error',)
Encountered QhullError on index 259 with args: ('Qhull error',)
Encountered QhullError on index 268 with args: ('Qhull error',) """



#TODO : Try your own clustering algorithm!

# == Abandoned Code ==

# make a convex hull of the flattened points so that you can identify the cluster edges on the 3d model
#testHull = sci_spatial.ConvexHull( flatClusters[15] )
#print testHull.vertices # DEBUG
#hullPoints = bounding_points_from_hull(testHull)
#fig3 = plt.figure()
#ax3 = fig3.add_subplot(111)
#xs,ys = split_to_2D( hullPoints )
#ax3.scatter(xs,ys)
#plt.show()

#edgeBases = [] # A list of basis vectors for each of the edges, used to populate the clusters with possible graps points
#edgeCenters = [] # A list of origins for each of the edges. These are chosen arbitrarily
#
#i = 0
#for edge in clusterEdges: # 'clusterEdges' will have the same length as 'clusters', but an edge may not be populated QHull envountered an error
#    edgeBases.append([])
#    clusLen = len(edge)
#    if clusLen > 0:
#        # choose two random points and use it as a basis vector
#        xVec = np_unit_vec( np.subtract( cluster[ random.randrange(clusLen/2) ] , cluster[ random.randrange(clusLen/2,clusLen) ] ) )
#        yVec = np_unit_vec( np.cross( clusterNorms[i], xVec ) ) # obtain a vector orthogonal both to the normal and the normal
#        # Place all the points in the cluster in an R2 plane
#        for point in cluster:
#            flatClusters[-1].append( [ np_vec_proj(point, xVec) , np_vec_proj(point, yVec) ] )
#    #else: emtpy cluster edge, qhull encountered an error while building it
#    i += 1

#for point in clusters[0]: # check to see if the points were in plane with the nucleus facet
#    w = np.subtract( point , planePnt )
#    dev = abs(np_vec_proj(w,curNorm))
#    print dev if dev > 0.00000001 else "flat", # All the points are on a plane
    
#testHull = sci_spatial.ConvexHull( clusters[0] )#, qhull_options="v Qbb" ) # FIXME : convex hull is not so convex!
#                                                  'qhull_options' "Wn" and "En" do not seem to improve it.
#                                                   Maybe need to repair or implement manually? Flatten to a plane, then run hull? (Transform twice) 
#                                                   What to use as plane basis?
#qhull precision warning: 
#The initial hull is narrow (cosine of min. angle is 0.9999999999999993).
#Is the input lower dimensional (e.g., on a plane in 3-d)?  Qhull may
#produce a wide facet.  Options 'QbB' (scale to unit box) or 'Qbb' (scale
#last coordinate) may remove this warning.  Use 'Pp' to skip this warning.
#See 'Limitations' in qh-impre.htm.



#print testHull.simplices # DEBUG

#xs,ys,zs = split_to_components( bounding_points_from_hull(testHull) )
#
##from mpl_toolkits.mplot3d import Axes3D # required on some distributions to use projection='3d'
#import matplotlib.pyplot as plt
#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(xs, ys, zs, c='b', marker='o')
#plt.show()

## Uncomment for plotting
#from mpl_toolkits import mplot3d
#from matplotlib import pyplot
#
## Create a new plot
#figure = pyplot.figure()
#axes = mplot3d.Axes3D(figure)
#
## and add the vectors to the plot
#axes.add_collection3d(mplot3d.art3d.Poly3DCollection(clusters))
#
## Auto scale to the mesh size
#scale = your_mesh.points.flatten(-1)
##axes.auto_scale_xyz(scale, scale, scale)
#axes.auto_scale_xyz(scale/2, scale/2, scale/2)
#
## Show the plot to the screen
#pyplot.show() # WORKS!   
#
## Create a new plot
#figure2 = pyplot.figure()
#axes = mplot3d.Axes3D(figure2)
#
## and add the vectors to the plot
#axes.add_collection3d(mplot3d.art3d.Poly3DCollection(testHull.simplices))
#
## Auto scale to the mesh size
#scale = your_mesh.points.flatten(-1)
##axes.auto_scale_xyz(scale, scale, scale)
#axes.auto_scale_xyz(scale/2, scale/2, scale/2)
#
## Show the plot to the screen
#pyplot.show() # WORKS!

## Plotting ##

# Uncomment for plotting
#from mpl_toolkits import mplot3d
#from matplotlib import pyplot

# Create a new plot
#figure = pyplot.figure()
#axes = mplot3d.Axes3D(figure)

# and add the vectors to the plot
#axes.add_collection3d(mplot3d.art3d.Poly3DCollection(your_mesh.vectors))

# Auto scale to the mesh size
#scale = your_mesh.points.flatten(-1)
#axes.auto_scale_xyz(scale, scale, scale)

# Show the plot to the screen
#pyplot.show() # WORKS!

## End Plotting ##

# == End Abandoned ==