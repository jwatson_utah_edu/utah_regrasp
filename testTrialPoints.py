# -*- coding: utf-8 -*-
"""
testTrialPoints.py , Built on Spyder for Python 2.7
James Watson, 2016 February
Test why the trial grasp points end up in space somewhere
"""

""" 
== LOG ==
2016-03-08: Fixed calculation of relative coordinated in the flattened plane
2016-03-03: Fixed basis vectors

== NOTES ==
* Use the magic command 
  %matplotlib
  to tell IPython to draw plots in separate windows. (Interactive 3D plots!)
  - Is there a way to set this in the script itself?
"""
# == Init Environment ==

# ~ PATH Changes ~ 
def localize(): # For some reason this is needed in Windows 10 Spyder (Py 2.7)
    """ Add the current directory to Python path if it is not already there """ 
    from sys import path # I know it is bad style to load modules in function
    import os.path as os_path
    containerDir = os_path.dirname(__file__)
    if containerDir not in path:
        path.append( containerDir )

localize() # You can now load local modules!

# ~ Standard Libraries ~
import math
from math import sqrt, ceil, sin, cos, tan, atan2, radians
from os import linesep
import random
# ~ Special Libraries ~
from mpl_toolkits.mplot3d import Axes3D # required on some distributions to use projection='3d', ignore "unused" warning
import matplotlib.pyplot as plt
import numpy as np
import scipy.spatial as sci_spatial 
# ~ Constants ~
EPSILON = 1e-7
# ~~ Shortcuts and Aliases ~~
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = linesep

# ~ Helper Functions ~

def eq(op1, op2):
    """ Return true if op1 and op2 are close enough """
    return abs(op1 - op2) <= EPSILON
    
def sep(title = ""):
    """ Print a separating title card for debug """
    LINE = '======'
    print LINE + ' ' + title + ' ' + LINE

# == End Init ==

# == Helpers Functions ==
# = Geo Functions =
def np_vec_proj(a,b):
    """ a projected onto b, a scalar length, using numpy """
    return np.dot(a,b) / np.linalg.norm(b)
    
def np_unit_vec(vec):
    """ Return a unit vector in the direction of 'vec', using numpy """
    return np.divide( vec , np.linalg.norm(vec) )
    
def split_to_components( vecList ):
    """ Separate a list of R3 vectors into three lists of components """ # because matplotlib likes it that way
    plotXs = []
    plotYs = []
    plotZs = []
    for vec in vecList:
        plotXs.append( vec[0] )
        plotYs.append( vec[1] )
        plotZs.append( vec[2] )
    return plotXs, plotYs, plotZs
    
def split_to_2D( vecList ): # ADDED TO PRODUCTION
    """ Separate a list of R2 vectors into two lists of components """ # because matplotlib likes it that way
    plotXs = []
    plotYs = []
    for vec in vecList:
        plotXs.append( vec[0] )
        plotYs.append( vec[1] )
    return plotXs, plotYs
    
def split_2d_on_XY_Zeq0( vecList ): # ADDED TO PRODUCTION
    """ Separate a list of R2 vectors into three lists of components on the X-Y plane, Z = 0 """ 
    plotXs = []
    plotYs = []
    plotZs = []
    for vec in vecList:
        plotXs.append( vec[0] )
        plotYs.append( vec[1] )
        plotZs.append(      0 )
    return plotXs, plotYs, plotZs
    
def bounding_points_from_hull(hull):
    """ Given a scipy.spatial.ConvexHull, return a list of the (supposedly) bounding points """
    # NOTE: Somehow the ConvexHull was not so convex?
    pntList = []
    # ConvexHull.vertices contains the indices of ConvexHull.points that are the bounding poly
    for index in hull.vertices: 
        pntList.append( hull.points[index] )
    return pntList
    
def cosd(angleDeg):
    """ Return the cosine of the angle, given in degrees """
    return cos( radians( angleDeg ) )
# = End Geo =

# = Plotting Helpers =
def plot_axes_3D_mpl(axes, scale): # ADDED TO PRODUCTION
    """ Display the coordinate axes in standard XYZ-RGB on a matplotlib 3D projection, each vector 'scale' in length """
    # NOTE: This function assumes that 'axes' has already been set up as a 3D projection subplot
    # URL, Show Origin: http://stackoverflow.com/a/11541628/893511
    # ax.plot( Xs    , Ys    , Zs    , c=COLORNAME )
    axes.plot( [0,scale] , [0,0    ] , [0,0    ] , c='r')
    axes.plot( [0,0    ] , [0,scale] , [0,0    ] , c='g')
    axes.plot( [0,0    ] , [0,0    ] , [0,scale] , c='b')
    
def plot_bases_3D_mpl(plotAX, origin, bX, bY, bZ, scale): # ADDED TO PRODUCTION
    """ Display the supplied axes in standard XYZ-RGB on a matplotlib 3D projection, each vector 'scale' in length """
    # NOTE: This function assumes that 'axes' has already been set up as a 3D projection subplot
    # NOTE: This function does not perform any checks whatsoever on the orthogonality or length of bX, bY, bZ
    # URL, Show Origin: http://stackoverflow.com/a/11541628/893511
    # ax.plot( Xs    , Ys    , Zs    , c=COLORNAME )
    Xs, Ys, Zs = split_to_components( [ origin , np.add( np.multiply( bX , scale ) , origin) ] )
    plotAX.plot( Xs, Ys, Zs , c='r')
    Xs, Ys, Zs = split_to_components( [ origin , np.add( np.multiply( bY , scale ) , origin) ] )
    plotAX.plot( Xs, Ys, Zs , c='g')
    Xs, Ys, Zs = split_to_components( [ origin , np.add( np.multiply( bZ , scale ) , origin) ] )
    plotAX.plot( Xs, Ys, Zs , c='b')
# = End Plotting =
# == End Helpers ==

flatPoly = ((0,0),(0,9),(9,9),(9,6),(6,6),(6,3),(9,3),(9,0))
origin = [5,5,5]

zBase = np_unit_vec([ 1, 1, 1])
yBase = np_unit_vec( np.cross( [0,0,1] , zBase ) )
xBase = np_unit_vec( np.cross(   yBase , zBase ) )


spacePoly = []
for point in flatPoly:
    #print point
    spacePoly.append( np.add( origin , 
                              np.add( np.multiply( xBase , point[0] ),
                                      np.multiply( yBase , point[1] ) ) ) )
sep('Orthogonal Test')
print np_vec_proj(xBase, yBase) # 0.0
print np_vec_proj(xBase, zBase) # 0.0
print np_vec_proj(yBase, zBase) # 0.0, The vectors constructed are orthogonal!
                                      
for point in spacePoly:
    #print point
    pass

sep('Flat Poly')
print sci_spatial.distance.euclidean(  flatPoly[0] ,  flatPoly[2] ) # 12.7279220614
print sci_spatial.distance.euclidean(  flatPoly[1] ,  flatPoly[7] ) # 12.7279220614
print sci_spatial.distance.euclidean(  flatPoly[0] ,  flatPoly[1] ) #  9.0
print sci_spatial.distance.euclidean(  flatPoly[1] ,  flatPoly[2] ) #  9.0
sep('Space Poly')
print sci_spatial.distance.euclidean( spacePoly[0] , spacePoly[2] ) # 12.7279220614
print sci_spatial.distance.euclidean( spacePoly[1] , spacePoly[7] ) # 12.7279220614, Corner to corner distances match!
print sci_spatial.distance.euclidean( spacePoly[0] , spacePoly[1] ) #  9.0
print sci_spatial.distance.euclidean( spacePoly[1] , spacePoly[2] ) #  9.0, Edge distances match!

plt.close('all') # clear any figures we may have created before
fig = plt.figure()
ax = fig.gca(projection='3d')#fig.add_subplot(111, projection='3d')

plot_axes_3D_mpl(ax, 2)

xs,ys,zs = split_to_components( [origin] )
ax.scatter(xs, ys, zs, c='red', marker='o')

xs,ys,zs = split_to_components( spacePoly )
ax.scatter(xs, ys, zs, c='k', marker='o')
ax.plot(   xs, ys, zs, c='k')

xs,ys,zs = split_2d_on_XY_Zeq0( flatPoly )
ax.scatter(xs, ys, zs, c='w', marker='o')
ax.plot(   xs, ys, zs, c='darkgrey') # URL, named colors: http://matplotlib.org/examples/color/named_colors.html

# URL, scale axes: http://stackoverflow.com/a/17996099/893511
ax.set_xlim( 0, 15)#set_xlim3d(-15, 15)
ax.set_ylim( 0, 15)#plt.ylim(-15, 15)
ax.set_xlim(-3, 15) # AttributeError: 'module' object has no attribute 'zlim'
plt.gca().set_aspect('equal')

plt.show()

# Recreate the trial point algorithm here to find out what is wrong
clusters = [spacePoly] # Pretend we have a model with only one face that has already been clustered
clusterNorms = [zBase]
# make a proper, planar convex hull of each of the clusters
i = 0 # keep track of the cluster index
flatClusters = [] # a collection of flattened clusters for computing convex hulls #  Each of these have a len(*) == len(clusters)              
flatBases = [] # a list of origins and basis vectors used for analyzing the clusters #      <'         /
flatBoxes = [] # a list of bounding boxes for each of the clusters, expressed in the cluster basis # <'
for cluster in clusters:
    clusLen = len(cluster)
    flatClusters.append( [] ) # append an empty list to be filled with flattened vectors
    # Create two basis vectors for this cluster
    # pick two random points in the cluster to be a basis vector, choose from two halves so that they are not the same
    xHead = cluster[ random.randrange(clusLen/2) ] # head of the x basis vector                                                             # ADDED TO PRODUCTION
    xTail = cluster[ random.randrange(clusLen/2,clusLen) ] # tail of the x basis vector, use as the origin for coordinate transformation    # ADDED TO PRODUCTION
    pOffset = np.subtract( xHead  , xTail ) # xTail , xHead                                                                                 # ADDED TO PRODUCTION
    xVec = np_unit_vec( pOffset ) # construct x unit basis vector based on two random points in our collection                              # ADDED TO PRODUCTION
    yVec = np_unit_vec( np.cross( clusterNorms[i], xVec ) ) # obtain a vector orthogonal both to the normal and the normal                  # ADDED TO PRODUCTION
    transfOffset = [ np_vec_proj(pOffset, xVec) , np_vec_proj(pOffset, yVec) ]                                                              # ADDED TO PRODUCTION
   #zVec = the normal vector for this cluster
    cXmax = -infty
    cXmin =  infty
    cYmax = -infty
    cYmin =  infty
    # Place all the points in the cluster in an R2 plane and find the bounding box
    for point in cluster:
        transformedX = np_vec_proj(np.subtract(point , xTail), xVec) # ADDED TO PRODUCTION
        transformedY = np_vec_proj(np.subtract(point , xTail), yVec) # ADDED TO PRODUCTION
        flatClusters[-1].append( [ transformedX , transformedY ] )
        cXmax = transformedX if transformedX > cXmax else cXmax
        cXmin = transformedX if transformedX < cXmin else cXmin
        cYmax = transformedY if transformedY > cYmax else cYmax
        cYmin = transformedY if transformedY < cYmin else cYmin
    flatBases.append( {'origin': xTail, 
                      'orgXDir': xHead,         # ADDED TO PRODUCTION
                         'xVec': xVec,          # ADDED TO PRODUCTION
                         'yVec': yVec,          # ADDED TO PRODUCTION
                         'zVec': clusterNorms[i],
                      'xOffset': pOffset } )    # ADDED TO PRODUCTION
    flatBoxes.append( ( ( cXmin , cYmin ) , ( cXmax , cYmax ) ) ) # ( ("lower-left" coords) , ("upper-right" coords) ) 
    i += 1 # increment index
  
for box in flatBoxes:
    xs, ys, zs = split_2d_on_XY_Zeq0( [ box[0] , [box[1][0],box[0][1]] ,  box[1] , [box[0][0],box[1][1]] , box[0] ] )
    ax.plot(   xs, ys, zs, c='darkred')
    
  
i = 0
errCount = 0
clusterEdges = [] # Collections of 3D points that represent the cluster boundaries
edges2D = [] # Collections of 2D points that represent the convex hull of a cluster projected onto a plane based on the constructed basis vectors
for fCluster in flatClusters:
    #if i == 14:
    #    break
    clusterEdges.append( [] ) # init a new cluster edge, NOTE: Note if there is an error below this list will be EMPTY
    edges2D.append( [] ) # init a new 2D edge, NOTE: Note if there is an error below this list will be EMPTY
    try: # attempt to form a convex hull from the flattened cluster, then select those cluster members at the edges and add them to 'clusterEdges'
        flatHull = sci_spatial.ConvexHull( flatClusters[i] ) # generate a convex hull of the flattened cluster
        for index in flatHull.vertices:
            clusterEdges[-1].append( clusters[i][index] ) # Add a point to the collection that represents the cluster boundary in 3D space
            edges2D[-1].append( flatClusters[i][index] ) #  Add a point to the collection that represents the cluster boundary in 2D space
    except Exception as ex: # URL, generic excpetions: http://stackoverflow.com/a/9824050
        print "Encountered" , type(ex).__name__ , "on index", i , "with args:", ex.args
        errCount += 1
    i += 1

print "Encountered", errCount, "errors while flattening clusters. There are ", len(clusters), "clusters.", \
       "{0:.2f}".format(errCount * 100.0 / len(clusters)) , "% error rate"
       
# Try to tile each of the clusters with N potential grasp points, rowsN*colsN

def elemw(i, iterable):
    """ Return the 'i'th index of 'iterable', wrapping to index 0 at all integer multiples of 'len(iterable)' """
    return iterable[ i % ( len(iterable) ) ]

def winding_num(point , polygon):
    """ Find the winding number of a point with respect to a polygon """
    # NOTE: Function assumes that the points of 'polygon' are ordered. Algorithm does not work if they are not
    # This algorithm is translation invariant, and can handle convex, nonconvex, and polygons with crossing sides.
    # This algorithm does NOT handle the case when the point lies ON a polygon side. For this problem it is assumed that
    #there are enough trial points to ignore this case
    # This works by shifting the point to the origin (preserving the relative position of the polygon points), and 
    #tracking how many times the positive x-axis is crossed
    w = 0
    v_i = []
    x = lambda index: elemw( index , v_i )[0] # We need wrapping indices here because the polygon is a cycle
    y = lambda index: elemw( index , v_i )[1]
    for vertex in polygon: # Shift the point to the origin, preserving its relative position to polygon points
        v_i.append( np.subtract( vertex , point ) )
    for i in range(len(v_i)): # for each of the transformed polygon points, consider segment v_i[i]-->v_i[i+1]
        if y(i) * y(i + 1) < 0: # if the segment crosses the x-axis
            r = x(i) + ( y(i) * ( x(i + 1) - x(i) ) ) / ( y(i) - y(i + 1) ) # location of x-axis crossing
            if r > 0: # positive x-crossing
                if y(i) < 0: 
                    w += 1 # CCW encirclement
                else:
                    w -= 1 #  CW encirclement
        # If one of the polygon points lies on the x-axis, we must look at the segments before and after to determine encirclement
        elif ( eq( y(i) , 0 ) ) and ( x(i) > 0 ): # v_i[i] is on the positive x-axis, leaving possible crossing
            if y(i) > 0:
                w += 0.5 # possible CCW encirclement or switchback from a failed CW crossing
            else:
                w -= 0.5 # possible CW encirclement or switchback from a failed CCW crossing
        elif ( eq( y(i + 1) , 0 ) ) and ( x(i + 1) > 0 ): # v_i[i+1] is on the positive x-axis, approaching possible crossing
            if y(i) < 0:
                w += 0.5 # possible CCW encirclement pending
            else:
                w -= 0.5 # possible  CW encirclement pending
    return w

def point_in_poly_w(point , polygon):
    """ Return True if the 'polygon' contains the 'point', otherwise return False, based on the winding number """
    return not eq( winding_num(point , polygon) , 0 ) # The winding number gives the number of times a polygon encircles a point

rowsN = 11 # number of rows of test points to generate
colsN = 11 # number of cols of test points to generate

index = 0 # cluster index
trialGraspPoints = []
for poly in edges2D:
    trialGraspPoints.append( [] ) # Add a list item even if the hull fails in order to maintain index parity
    if len(poly) > 0: # if we were actually able to construct a hull
        #         flatBoxes[index] : ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
        rowSpan = abs( flatBoxes[index][1][1] - flatBoxes[index][0][1] ) / (rowsN - 1) # height of a row
        colSpan = abs( flatBoxes[index][1][0] - flatBoxes[index][0][0] ) / (colsN - 1) # width of a col
        # Create a grid of test points that span the bounding box, and keep the ones within the convex hull as potential grasp points
        for i in range(1,rowsN - 1): # for every row of test points         
            for j in range(1,colsN - 1): # for every column of test points  
                testX = flatBoxes[index][0][0] + colSpan * j
                testY = flatBoxes[index][0][1] + rowSpan * i
                if eq(testY , 0):
                    testY = rowSpan/10.0
                testPoint = ( testX , testY )
                xs , ys , zs = split_2d_on_XY_Zeq0( [testPoint] )
                ax.scatter( xs , ys , zs , c='darkred', marker='o', s=14 ) 
                if point_in_poly_w( testPoint , poly ): # the point is inside the flattened hull, create the trial grasp in 3D space on the model
                    #flatBases[i] = {'origin': xTail, 'xVec': xVec , 'yVec': yVec, 'zVec': clusterNorms[i] }
                    ax.scatter( xs , ys , zs , c='darkred', marker='x', s=70 )
                    trialGraspPoints[-1].append( np.add( flatBases[index]['origin'],                                            # ADDED TO PRODUCTION
                                                         np.add( np.multiply( flatBases[index]['xVec'] , testPoint[0]  ) ,      # ADDED TO PRODUCTION
                                                                 np.multiply( flatBases[index]['yVec'] , testPoint[1]  ) ) ) )  # ADDED TO PRODUCTION  
    index += 1 # increment cluster index

i = 0
for face in trialGraspPoints:
    xs,ys,zs = split_to_components( face )
    ax.scatter(xs, ys, zs, c='blue', marker='o', s=14)
    i += 1

i = 0
for base in flatBases:
    xs,ys,zs = split_to_components( [base['origin']] )
    ax.scatter(xs, ys, zs, c='red', marker='x', s=100) 
    xs,ys,zs = split_to_components( [base['orgXDir']] )
    ax.scatter(xs, ys, zs, c='blue', marker='x', s=100)
    xs,ys,zs = split_to_components( [ base['origin'] , base['orgXDir'] ] )
    ax.plot(   xs, ys, zs, c='darkblue')
    i += 1

xs,ys,zs = split_to_components( [origin] )
ax.scatter(xs, ys, zs, c='black', marker='x', s=100)
plot_bases_3D_mpl(ax, flatBases[0]['origin'], flatBases[0]['xVec'], flatBases[0]['yVec'], flatBases[0]['zVec'], 2)

sep('Orthogonal Test')
print eq( np_vec_proj(flatBases[0]['xVec'], flatBases[0]['yVec']) , 0 ) # True
print eq( np_vec_proj(flatBases[0]['xVec'], flatBases[0]['zVec']) , 0 ) # True
print eq( np_vec_proj(flatBases[0]['yVec'], flatBases[0]['zVec']) , 0 ) # True, The vectors constructed are orthogonal!

"""
== Diagnosis ==
2016-03-07: Found an error in the calculation of relative points in the flattened plane. The error allowed test points
            in the same erronous frame to be correctly placed inside or outside the hull, but caused the final position
            of the points to be offset from their intended positions by a random offset determined by the two points 
            randomly chosen to be the X basis vector for the flattened plane.
2016-03-03: * The test points seem to be in the general shape of the poly hull, and also in plane of the poly hull.  The
              problem seems to be that the points are all shifted away from the polygon itself, but in a seemingly random
              direction.  Since the basis X vector is chosen at random, the problem likely has to do with an offset along
              this or the basis Y vector.
            * There also seem to be some points outside where one would expect, especially at the corners. How does the
              winding algorithm deal with points on the border of the poly? This seems to happen when a row of points
              begins at a corner and the random basis is oblique to the poly edge

"""