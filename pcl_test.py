#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Template Version: 2016-05-10

# future first
from __future__ import division # Future imports must be called before everything else!

"""
pcl_test.py , Built on Spyder for Python 2.7
James Watson, 2016 June
Test Point Cloud Library (PCL) for regrasp
"""

"""
=== NOTES ===
* This code assumes that the convex hull will always be composed of triangular faces. If this turns out not to be the case,
  or if clusters are implemented for the hull. changes will have to be made to the below
* If the sparce documentation for the Python bindings do not help, look at the source, examples, and unit tests
  Source:     https://github.com/strawlab/python-pcl/blob/master/pcl/_pcl.pyx
  Unit Tests: https://github.com/strawlab/python-pcl/blob/master/tests/test.py
  Examples:   https://github.com/strawlab/python-pcl/tree/master/examples
"""

# FIXME:
""" The method below still predicts that there are faces of the hull on curves that will support the object. There may be
several ways to address this:
  1. Besides stability (closeness of projected COM to edge), the area of the supporting face could also be taken into account.
     A broad, flat face may support the object better than a short and/or narrow face that is part of a curve. 
  2. Augmenting 1 with clustering of the simplified model before producing a hull so that the desired broad, flat faces
     will emerge. """


# == Init Environment ==


# ~ PATH Changes ~ 
def localize(): # For some reason this is needed in Windows 10 Spyder (Py 2.7)
    """ Add the current directory to Python path if it is not already there """
    from sys import path # I know it is bad style to load modules in function
    import os.path as os_path
    containerDir = os_path.dirname(__file__)
    if containerDir not in path:
        path.append( containerDir )

localize() # You can now load local modules!

# ~ Standard Libraries ~
import math
from math import sqrt, ceil, sin, cos, tan, atan2, radians
import os
import datetime
import cPickle
# ~ Special Libraries ~
import matplotlib.pyplot as plt
import numpy as np
from stl import mesh # https://pypi.python.org/pypi/numpy-stl/ # pip install numpy-stl
import pcl # pcl is installed
from mesh_helpers import *
# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = os.linesep
SOURCEDIR = os.path.dirname(os.path.abspath(__file__)) # URL, dir containing source file: http://stackoverflow.com/a/7783326

# ~ Helper Functions ~

def eq(op1, op2):
    """ Return true if op1 and op2 are close enough """
    return abs(op1 - op2) <= EPSILON
    
def sep(title = ""):
    """ Print a separating title card for debug """
    LINE = '======'
    print LINE + ' ' + title + ' ' + LINE
    
nowTimeStamp = lambda: datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') # Return a string of the date and time down to second

plt.close('all') # clear any figures we may have created before

# == End Init ==

""" Currently Python bindings for PCL do not include loading of STL files """

def STL_to_PCD(STLpath, toPCDfile = False, overwrite = False):
    """ Convert an STL file to a Point Cloud Data file """
    STLdir, STLfile = os.path.split(STLpath) # Fetch path info for the STL file
    PCDfile = STLfile[:-4] + '.pcd' # Construct a PCD filename that will be generated from the STL
    PCDpath = os.path.join( 'output' , PCDfile ) # Construct an output path for the PCD file
    print STLdir, STLfile 
    
    cloud = pcl.PointCloud() # Instantiate a point cloud data object
    
    if overwrite or not os.path.isfile(PCDpath): # If the file does not exist yet or we want to overwrite it, create and populate the PCD
        #pntList = uniq_pts_from_stl(STLpath) 
        pntList = all_pts_from_stl(STLpath) # By using all points from all facets, 6X the unique points are collected
        #                                     but the results are very quick. It takes several minutes to determine unique points
        
        cloud.from_list(pntList) # populate the point cloud object with points from the STL
        if toPCDfile or overwrite: # If the clien code wants a file to be made, create the dile
            cloud.to_file( PCDpath )  
    else: # else a file already exists and we do not want to overwrite it
        cloud.from_file( PCDpath ) # Fetch PC data from a previous run
    
    return cloud
    
theFileName = "spam_simplified.stl"
spamCloud = STL_to_PCD(theFileName, True)

spamPoints = spamCloud.to_array()
spamSpan = span_3D( bound_box_3D( spamPoints ) )
print "Span of the spam is", spamSpan
leaf = min(spamSpan) * 0.1 # For now use this as the simplification metric
# min(spamSpan) * 0.1 : for clustering
# min(spamSpan) * 0.2 : for stability

simpleFilter = spamCloud.make_voxel_grid_filter() #pcl.VoxelGridFilter(spamCloud)

simpleFilter.set_leaf_size(leaf, leaf, leaf)
simpleCloud = simpleFilter.filter()

print len( spamPoints ) # 2002 elems
#plot_points_only_list( spamPoints )
simplePoints = simpleCloud.to_array()
print len( simplePoints ) # 361 elems at leaf = 0.01
#plot_points_only_list( simplePoints )

simpleHull = sci_spatial.ConvexHull( simplePoints ) 
print "Hull of simple cloud has", len(simpleHull.simplices), "faces" # 414
print "Hull of simple cloud has", len(simpleHull.vertices), "points,", len( simplePoints ) - len(simpleHull.vertices), "are enveloped" # 209 , 152

""" Goal: Use facets of a simplified convex hull to determine stable poses of an object

Right now, the code is based on projecting the center of mass onto an cluster. After making this determination, the code simply lists the clusters 
that are "suitable" for supporting the object.

The new code must use facets of the simplified convex hull, and must return information about how the object is supported without relying on the cluster information
already generated. The information that must be stored for each pose identified is
  1. The extent of the supporting facet (coordinates of points (in order?))
  2. The normal vector of the facet
  3. The location of the projected COM on the facet
  4. The closest distance to an edge of the facet
 """

def identify_stable_poses(R3PointsList, COM):
    """ Given a simplified point cloud 'R3PointsList', determine the stable poses that will support it, given COM """ 
    
    simpleHull = sci_spatial.ConvexHull( R3PointsList ) # construct a convex hull of the simplified model.
    # Once the qhull has constructed the hull, it will already have a list of facets which it refers to as simplices
    
    rtnPoses = [] # A list of poses, each with the required pieces of information explained above
    
    for face in simpleHull.simplices:
        
        if len(face) != 3: # Check to see if any of the hull faces have other than 3 points
            print "This face has", len(face), "points" # No faces for this hull have other than 3 points
        
        # 1. The extent of the supporting facet (coordinates of points (in order?))
        # 'face' : [319 354 172] , list of indices of 'simpleHull.points', unsure if they are in a particular order
        orderCorrect = True # First assume that the vertices are given in CCW order and then see how far you get
        
        # 2. The normal vector of the facet
        vecA = np.subtract( simpleHull.points[ face[1] ] , simpleHull.points[ face[0] ] ) # form A
        vecB = np.subtract( simpleHull.points[ face[2] ] , simpleHull.points[ face[1] ] ) # form B
        faceNorm = np_unit_vec( np.cross(vecA , vecB) ) # form the (unit) normal A cross B, this is either the normal or its opposite
        
        # Now we need to know whether this is the normal vector or its opposite. For a convex shape, the normal always
        #points away from the centroid of the hull/object.
        
        outwardVec = np.subtract( simpleHull.points[ face[0] ] , COM ) # construct a vector from the COM to this face
        
        if np.dot(faceNorm, outwardVec) < 0: # The dot product of these two will be positive if they are facing in the same direction
            faceNorm = np.multiply(faceNorm, -1) # Otherwise the normal is opposite of what it should be
            orderCorrect = False # points must be put in CCW order before they are reported
        
        # For the 2D projection, pick the projection of model's z as the plane's y
        zVec = faceNorm # z basis vector
        yVec = np_unit_vec( np_proj_vec_to_plane( [0,0,1] , zVec ) ) # y basis vector
        xVec = np.cross(yVec, zVec) # x basis vector
        # Pick the geometric center of the triangle as the origin
        triCenter = vector_mean( simpleHull.points[ face[0] ] , simpleHull.points[ face[1] ] , simpleHull.points[ face[2] ] )
        
        flatFace = [] # List of the face boundary points, expressed in a 2D plane
        
        # Make sure the points are CCW order as observed from the exterior
        if orderCorrect:
            planePoints = [simpleHull.points[ face[0] ] , simpleHull.points[ face[1] ] , simpleHull.points[ face[2] ]]
        else: 
            planePoints = [simpleHull.points[ face[0] ] , simpleHull.points[ face[2] ] , simpleHull.points[ face[1] ]]
            
        for point in planePoints:
            flatFace.append( np_basis_change_3D(point, triCenter, xVec, yVec, zVec)[:2] ) # only need the x-y coords
        
        # 3. The location of the projected COM on the facet
        projectedCOM = np_proj_vec_to_plane( np.subtract(COM , triCenter) , faceNorm) # Project the center of mass onto the the face under scrutiny   
        surfaceCOM = np_add( projectedCOM , triCenter )
        # The projected center of mass is already offset from the triangle center
        transformedCOM = np_basis_change_3D(projectedCOM, [0,0,0], xVec, yVec, zVec)[:2] # only need the x-y coords
        
        facetStable = point_in_poly_w(transformedCOM , flatFace) # is the projection of the COM within the hull face?
        
        if facetStable: # If the projection of the center of mass is within the hull face
            
            # 4. The closest distance to an edge of the facet
            shortestDist = infty
            for index, point in enumerate( flatFace ): # for every line segment in the facet, comput distance from project COM to segment
                temp = d_point_to_segment_2d( transformedCOM , elemw(index, flatFace), elemw(index+1, flatFace) )
                if temp < shortestDist: 
                    shortestDist = temp # Store the shortest distance to any segment
                    
            # Package all the the desired information as a tuple and append to the list of stable faces
#           1. The extent of the supporting facet (coordinates of points (in order?))
#           2. The normal vector of the facet
#           3. The location of the projected COM on the facet
#           4. The closest distance to an edge of the facet
            rtnPoses.append( # FIXME: START HERE
                (
                    tuple( [ simpleHull.points[ face[0] ] , simpleHull.points[ face[1] ] , simpleHull.points[ face[2] ] ]) if orderCorrect else \
                        tuple( [ simpleHull.points[ face[0] ] , simpleHull.points[ face[2] ] , simpleHull.points[ face[1] ] ] ),
                    tuple(faceNorm),
                    tuple(surfaceCOM),#tuple(projectedCOM),
                    shortestDist
                )             
            )
                    
        # else the facet is not stable, do not add it to the list of stable facets
    return rtnPoses

# 1. Produce a COM for a collection of points
COM = volume_centroid_of_points(simplePoints)

# 2. Produce a list of stable hull facets to support the object
supportingFacets = identify_stable_poses(simplePoints, COM)
print "Found", len(supportingFacets), "supporting hull faces"
for item in supportingFacets:
    print item, endl

# 3. Display the collection of points along with stable facets and projected COM for each stable facet
ptsList = simplePoints
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

xs,ys,zs = split_to_components( ptsList )
ax.scatter(xs, ys, zs, c='blue', marker='o', s=14)
plot_axes_3D_mpl(ax, scale = 0.05)

offset = 0
divisor = 1
basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
numColors = len(basicColors)

for i, edge in enumerate( [edge[0] for edge in supportingFacets] ): # Plot edges as closed polygons
    if len(edge) > 0 and (i+offset) % divisor == 0:
        xs, ys, zs = split_to_comp_cycle( edge )
        ax.plot(xs, ys, zs, c = basicColors[i%(numColors-1)])
        
for i,projCOM in enumerate( [edge[2] for edge in supportingFacets] ):
    xs, ys, zs = split_to_components( [projCOM] )
    ax.scatter(xs, ys, zs, c = basicColors[i%(numColors-1)], marker='x', s=140)

plt.gca().set_aspect('equal')
plt.show()

# == Notes and Abandoned Code ==========================================================================================

# Try to see if expanding the model
#bigPoints = [np.multiply(elem , 10000.0) for elem in simplePoints]
#bigHull = sci_spatial.ConvexHull( bigPoints ) 
#print "Hull of big cloud has", len(bigHull.simplices), "faces" # 414, increasing the spacing of points did not reduce hull facets

#for eqn in simpleHull.equations:
#    print eqn

#for face in simpleHull.simplices:
#    print face