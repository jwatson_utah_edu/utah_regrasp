#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
mesh_general.py
James Watson, 2016 March
Analyze grip location on a general STL file, expansion of "mesh_test.py"

~~ Dependencies ~~
numpy :     To support numpy-stl
numpy-stl : To process STL files , https://pypi.python.org/pypi/numpy-stl/ , pip install numpy-stl
scipy :     To calculate nearest neighbors in point clouds
"""

"""
== LOG ==
2016-03-19: The following files only produce once cluster each, probably the entire model exists within the margin of the cluster aggregator
            - pssn_stl/2inMetaWash.stl
            - pssn_stl/1AndAHalfInMetaWash.stl
            - pssn_stl/MoutSecuSteeShacKey.stl
2016-03-18: Batch processing of STL files seems to work
2016-03-17: The use of the tiled points on the clusters is somehow not working. Clusters so far are very small, so using
            the centroids of clusters exclusively as grasp points. Average ~300 pairs for the simplified spam can
2016-03-16: Projects COM onto each of the clusters and determine stability of pose
            - Clusters really aren't an appropriate measure for pose stability. For example, if a four-legged chair is
              being evaluated, there is probably at least one cluster underneath the seat of the chair within which the
              chair center of mass might be projected. However, the chair would never be resting on this surface because
              it would be resting on its four legs instead.
            - Need a better convex hull -or- a way to identify which clusters are outermost and therefore candidates for
              a resting plane for a pose
2016-03-15: Computes volume centroid
2016-03-12: * Added benchmarking code
            * Initial nucleating node for clustering is now randomly chosen
2016-03-10: * Organized code into semi-logical functions, performs identically to pre-organized code
              - Should now be generalized to accept any STL model
            * Defined a dictionary structure for mesh/point information
2016-03-08: * Clusters are populated with trial grasp points!
            * Moved "mesh_test.py" code to "mesh_general.py" and began organizing code
2016-02-20: Flattened clusters yield clean convex hulls.
2016-02-18: Added an angle condition to the facet acceptance criteria
2016-02-14: Tested basic point operations in numpy-stl

== TODO ==
* Scale the cluster plane distance criterion to the specific model so that small models do not get flattened to a single cluster
* Convert all the out-of-loop 'i' counter patterns to an enumerate pattern
  I hope this is true: 'enumerate' does not copy list arg - http://stackoverflow.com/a/23015166/893511
* Test the demo on models OTHER than spam!
* give weights to both the facet center distance and the normal separation angle so that they are not straigh pass-fail 
  criteria
  
== NOTES ==
* STL file coordinates are scaled to meters, although STL files do not have explicit scale/size data other than raw coords
* Use the magic command 
  %matplotlib 
  to tell IPython to draw plots in separate windows. (Interactive 3D plots!)
  - Is there a way to set this in the script itself?
  - Vanilla Python does not need to be told this
* Vanilla Python is giving more detailed error information for 'qhull'
  
== IDEAS ==
* Throw away STL facets to make less work on the grasp planner. (Do I have to do this in a smart way?)
"""
import traceback
from mesh_helpers import *

""" == Structure of an STL file ==
# URL: https://en.wikipedia.org/wiki/STL_%28file_format%29#Binary_STL

#Each triangle is described by twelve 32-bit floating-point numbers: three for the normal and then three for the X/Y/Z 
#coordinate of each vertex. After these follows a 2-byte ("short") unsigned integer that is the "attribute byte count" – 
#in the standard format, this should be zero because most software does not understand anything else.

    == End STL == """
    

""" == Structure of Processed STL Mesh ==
'load_and_cluster_STL' takes as an argument the path to an STL file, attempts to load it, create a mesh.Mesh object, 
then proceeds to generate candidate grasp points on clustered faces. It returns a dictionary with the following structure.
Each of the lists in the structure has the same length of 'clusters', even if an operation on a cluster is unsuccessful.
In this case the list produced by the operation will be empty. Lists must have identical lengths, functions depend on the
results of each per-cluster operation having the same index as the associated cluster.
{
    'mesh' :              mesh.Mesh object generated from 'meshPath'
    'clusters' :          List of STL points that have been flattened onto clusters
    'clusterNorms' :      List of normal vectors for each of the clusters in 'clusters'
    'flatBases' :         Dictionary of basis vectors for each of the clusters, with the X-Y plane aligned with the cluster plane
    'flatBoxes' :         Bounding box of clustered points expressed in 'flatBases': ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
    'clusterEdges' :      List of cluster edges, each edge is a list of ordered points in the STL bases
    'edges2D' :           List of cluster edges, each edge is a list of ordered points in 'flatBases'
    'trialGraspPoints' :  List of trial grasp tilings, each is a list of 3D points associated with a cluster
                          2016-04-28: As of right now, any cluster that is part of an approved grasp pair will have the 
                                      centroid of the of the cluster appended to the list of trial points associated with 
                                      the cluster. Assume that the last element is the point to grasp / place a finger at
    'volumeCentroid' :    3D point signifying the volume centroid of the 'mesh'. Stand-in for COM assuming uniform desnity
    'clusterStability':   Dictionary with information about whether or not a cluster is an appropriate resting face for a pose
    'approvedGraspPairs': List of tuples denoting items of 'trialGraspPoints' that are suitable for grasping given criteria
                          As described: ( (index of cluster 1 , index of centroid in cluster 1's grasp candidates) , 
                                          (index of cluster 2 , index of centroid in cluster 2's grasp candidates) )
}
The generation of the above structure is separated into different operations: 
    Load, 
    Cluster, 
    Find Edges, 
    Tile With Points,
    Locate Volume Centroid,
    Assess pose stability for each cluster
    Choose best pairs from tilings of clusters
    == End Mesh == """

def load_and_cluster_STL(STLfilePath, handPoints): # 'spam_simplified.stl'

    your_mesh = mesh.Mesh.from_file(STLfilePath) # ---------------------------------------------------------------------------- Load the STL file
    dbgLog(1, "DEBUG: Load success!" ) # If this line reached, then model loaded
    clusters , clusterNorms , triCenters = cluster_mesh(your_mesh) # ---------------------------------------------------------- Cluster
    flatBases , flatBoxes , clusterEdges , edges2D = clusters_to_egdes(clusters, clusterNorms) # ------------------------------ Find cluster edges
    trialGraspPoints = tile_clusters_with_trial_points(edges2D, flatBoxes, flatBases) # --------------------------------------- Tile clusters with grasp points
    volumeCentroid = volume_centroid_of_points(triCenters) # ------------------------------------------------------------------ Find mesh centroid
    poseStability = pose_stability(volumeCentroid, your_mesh) # --------------------------------------------------------------- Stability of poses
    approvedGraspPairs = ID_grasp_points_from_tilings(clusterNorms, trialGraspPoints, flatBases, edges2D, flatBoxes) # -------- ID grasp points passing criteria
    #graspsForPoses = prune_grasps_for_poses( poseStability , approvedGraspPairs , trialGraspPoints , flatBases , handPoints ) # ID candidate grasps per pose

    return {
        'mesh' :              your_mesh,
        'triCenters' :        triCenters,
        'clusters' :          clusters,
        'clusterNorms' :      clusterNorms,
        'flatBases' :         flatBases,
        'flatBoxes' :         flatBoxes,
        'clusterEdges' :      clusterEdges,
        'edges2D' :           edges2D,
        'trialGraspPoints' :  trialGraspPoints,
        'volumeCentroid' :    volumeCentroid,
        'clusterStability':   poseStability, # TODO: Change the key to read 'poseStability'
        'approvedGraspPairs': approvedGraspPairs #,
        #'graspsForPoses':     graspsForPoses
    }
    

def cluster_mesh(your_mesh): # returns clusters , clusterNorms
    """ Create clustered facets in a similar fashion to Harada """
    # Grasp Planning for Parallel Grippers with Flexibility on its Grasping Surface, Kensuke Harada
    dbgLog(1, "DEBUG:", "Clustering..." )

    # This is slightly different than the clustering method presented in the above. Instead of picking pairs of facets, I 
    #just ask the k-D tree for the N nearest neighbors to the node under scrutiny. These facets are added to the cluster in 
    #order until one of them fails the height test. The facet that failed will become the nucleus of a new cluster

    sep( "Attempt clustering of the object surface" )

    # = Clustering Init =
    totalNodes = len(your_mesh) # get the number of triangles in the mesh
    #curNode = random.randrange(totalNodes) # 
    curNode = random.randrange(totalNodes) # retrieve index of a random node 
    #print "DEBUG: Chose the node at index", curNode, "from",totalNodes,"total model points"
    hLimit = 0.01 #0.001 # The height limit for clustering # NOT EVEN SURE WHAT SCALE?
    pLimit = cosd( 30 )  # projection limit, any facet that has a unit normal more than 7 degrees away from the nuclues unit 
    #                      normal will be rejected
    #usedFacets = set([]) # set of facet indices that have become part of a cluster
    unusedFacets = set(range(totalNodes)) # set of facet indices that have NOT become part of a cluster
    clusterLimit = 750 #500 #250 #1000 # the maximum number of facets that will be considered, per cluster
    triCenters = tri_centers(your_mesh) # get the coords of every facet center in the model
    nearNeighbors = sci_spatial.cKDTree(triCenters, leafsize=100) # generate k-D tree of nearest neighbors
    clusters = [] # list of clusters, new pseudo-faces for calc of candidate grasp
    clusterNorms = [] # the unit normal vectors for all clusters created
    #nuChosen = True # flag is true if a nucleus facet has been chosen for the next cluster
    # = End Init =

    iterCount = 0

    while len(unusedFacets) > 0: # while there are unclustered points in the model, create a new cluster
    
        iterCount += 1
        
        lastNode = curNode # store the index of the last nucleus in the model
        
        # per new cluster
        #curNeighbors = []
        curNeighbors = nearNeighbors.query(triCenters[curNode], k=clusterLimit, distance_upper_bound=6) # get the 'clusterLimit' nearest neighbors to current node
        addCount = notCount = 0 # init count of clustered nodes
        clusters.append( [ your_mesh.v0[curNode] , your_mesh.v1[curNode] , your_mesh.v2[curNode] ] ) # add points of the nucleus to the cluster, guaranteed one
        unusedFacets.remove(curNode) # the nucleus facet is removed, it is part of a cluster even if it is the only member
        #print "Began a new cluster at",curNode,", removed.", # DEBUG
        curNorm = np_unit_vec( your_mesh.normals[curNode] ) # get the normal of the nucleus facet for this cluster, convert to unit, to define the plane
        clusterNorms.append( curNorm ) # store the unit vector for this cluster
        planePnt = triCenters[curNode] # get a reference point on the nucleus facet so that we can calc per distance
        
        #lastLoopAdd = 0 # DEBUG
        #lastLoopSkip = 0 # DEBUG: How many we would have skipped were we to keep searching after first failure
        
        clusterEnd = False # fresh cluster, perform all aggregating activities
        
        #print "DEBUG: Begin a new cluster at", curNode
        
        for facet in xrange(1, clusterLimit): # per neighbor facet in the list of nearest neighbors
            #print "DEBUG: Unused facets:",unusedFacets
            #print "DEBUG: Number of clusters",len(clusters)
            facDex = curNeighbors[1][facet] # index of this neighbor in the list of mesh points, where we are in the mesh
            if facDex < totalNodes:
                #print "Accessing facet",facDex,"out of",totalNodes,". Iteration",iterCount
                #print "Debug , cluster_mesh: Working with facet",facDex
                if not clusterEnd: # only perform calcs if 
                    w = np.subtract( triCenters[facDex] , planePnt ) # separating vector between trial facet and the nucleus reference point
                    curH = abs(np_vec_proj(w,curNorm)) # calculate the perpendicular separation distance between the facet center and the nucleus plane
                    facetNorm = np_unit_vec( your_mesh.normals[facDex] ) # get the unit normal of the facet under scrutiny
                    curP = np_vec_proj(facetNorm,curNorm)
                # TODO : consider updating the nucleus plane to some average of normals/points after every N facets have been processed.
                #        This way a nucleus that is "off the true local plane" does not cause the cluster to skip over a naturally flat 
                #        surface. [WOULD AN AVERAGE OF CENTERS BE ON THE NEW PLANE? TEST!]
                #print curH, "\t",
                if not clusterEnd and curH < hLimit and curP > pLimit: # no prev test failed, 
                #                                                        perp distance between neighbor center and nucleus is within limit, 
                #                                                        angle between nucleus and facet is below limit, add to cluster
                    #print "DEBUG: Add the facet"
                    if facDex in unusedFacets: # if the current facet has not been assigned to a cluster yet
                        #print "Added",facDex,".", # DEBUG
                        addCount += 1 
                        #lastLoopAdd = facet # DEBUG
                        unusedFacets.remove(facDex) # mark this facet as clustered
                        #print "DEBUG: Removed facet at index", facDex, "in mesh."
                        #threePoints =[] # used to structure vertices of a facet as one list
                        for point in [ your_mesh.v0 , your_mesh.v1 , your_mesh.v2 ]: # for each vertex of the facet
                            w = np.subtract( point[facDex] , planePnt ) # generate a separation vector with respect to the nucleus reference
                            curH = np_vec_proj(w,curNorm) # calc the perp separation from the vertex to the cluster plane
                            clusters[-1].append( np.subtract( point[facDex] , np.multiply( curNorm , curH ) ) ) # append point projected onto nucleus plane
                    else:
                        #print "Skipped",facDex,"for adding.", # DEBUG
                        pass
                    #else: We encountered a used facet but have not yet failed a distance test, continue aggregating facets
                else: # else neighbor is too far from nucleus plane, do not add to cluster, break
                    #print "DEBUG: Do not add facet"
                    clusterEnd = True # switch the loop to a mode in which it searches for the next cluster nucleus
                    notCount += 1
                    #lastLoopSkip = facet # DEBUG
                    if facDex in unusedFacets:
                        curNode = facDex # get the mesh index of the first point that failed to be clustered, this will be the nucleus of the next iteration
                        #curNode = curNeighbors[1][facet] # get the mesh index of the first point that failed to be clustered, this will be the nucleus of the next iteration
                        #print "Located new nucleus at",facDex,".\n" # DEBUG
                        break # stop building this cluster on the first addition failure
                    else: #continue to the next closest node.  
                        #print "Skipped",facDex,"for new nucleus.", # DEBUG
                        # We have not removed used nodes from the neighborhood calc above, so facets at cluster boundaries could potientially be iterated over many times
                        # TODO : SHOULD I EVEN SEARCH NEIGHBORS AT THIS POINT?  WHAT DO I HAVE TO LOSE FROM NUCLEATING A CLUSTER AT A RANDOM LOCATION?
                        pass
                        
                    # If the above 'break' statement is uncommented, then less facets are added to the cluster. This is because there
                    #are some facets that are outside the cluster criteria that have farther neighbors that are within the cluster 
                    #criteria. Continuing to search after the criteria fails results in a wider cluster, but does not leave an automatic
                    #way to include the skipped nodes in the current cluster, nor to take any interior cluster into account
            # else we somehow arrived at an index outside of range, fetch another (Happens for 4% of models)
        # If we are here and 'curNode' has not changed, it means we exhausted all the options in the nearest neighbors for a nucleus, we have to pick from the 
        #remaining unused nodes throughout the model. Hopefully they are not spread out all over?
        if curNode == lastNode and len(unusedFacets) > 0:
            curNode = unusedFacets.pop() # return an arbitrary facet # URL: https://docs.python.org/2/library/stdtypes.html#set.pop
            unusedFacets.add(curNode) # Python sets are not indexable, and pop is destructive, so we have to add this index back on for the sake of the next loop, 
            #                           which will remove it
    dbgLog(1, "Debug:",iterCount,"iterations while clustering." )
    return clusters , clusterNorms , triCenters
            

def clusters_to_egdes(clusters, clusterNorms): # returns flatBases , flatBoxes , clusterEdges , edges2D
    """ make a proper, planar convex hull of each of the clusters, no fuzzy edges this time, please """
    dbgLog(1, "DEBUG:", "Flattening clusters..." )
    
    i = 0 # keep track of the cluster index
    flatClusters = [] # a collection of flattened clusters for computing convex hulls #  Each of these have a len(*) == len(clusters)              
    flatBases = [] # a list of origins and basis vectors used for analyzing the clusters #      <'         /
    flatBoxes = [] # a list of bounding boxes for each of the clusters, expressed in the cluster basis # <'
    for cluster in clusters:
        clusLen = len(cluster)
        flatClusters.append( [] ) # append an empty list to be filled with flattened vectors
        # Create two basis vectors for this cluster
        # pick two random points in the cluster to be a basis vector, choose from two halves so that they are not the same
        xHead = cluster[ random.randrange(clusLen/2) ] # head of the x basis vector                                                             
        xTail = cluster[ random.randrange(clusLen/2,clusLen) ] # tail of the x basis vector, use as the origin for coordinate transformation    
        pOffset = np.subtract( xHead  , xTail ) # xTail , xHead
        xVec = np_unit_vec( pOffset ) # construct x unit basis vector based on two random points in our collection
        yVec = np_unit_vec( np.cross( clusterNorms[i], xVec ) ) # obtain a vector orthogonal both to the normal and the normal
        #transfOffset = [ np_vec_proj(pOffset, xVec) , np_vec_proj(pOffset, yVec) ]
       #zVec = the normal vector for this cluster
        cXmax = -infty
        cXmin =  infty
        cYmax = -infty
        cYmin =  infty
        # Place all the points in the cluster in an R2 plane and find the bounding box
        for point in cluster:
            transformedX = np_vec_proj(np.subtract(point , xTail), xVec) 
            transformedY = np_vec_proj(np.subtract(point , xTail), yVec) 
            flatClusters[-1].append( [ transformedX , transformedY ] )
            cXmax = transformedX if transformedX > cXmax else cXmax
            cXmin = transformedX if transformedX < cXmin else cXmin
            cYmax = transformedY if transformedY > cYmax else cYmax
            cYmin = transformedY if transformedY < cYmin else cYmin
        flatBases.append( {'origin': xTail, 
                          'orgXDir': xHead,         
                             'xVec': xVec,          
                             'yVec': yVec,          
                             'zVec': clusterNorms[i],
                          'xOffset': pOffset } )    
        flatBoxes.append( ( ( cXmin , cYmin ) , ( cXmax , cYmax ) ) ) # ( ("lower-left" coords) , ("upper-right" coords) ) 
        i += 1 # increment index
    
    i = 0
    errCount = 0
    clusterEdges = [] # Collections of 3D points that represent the cluster boundaries
    edges2D = [] # Collections of 2D points that represent the convex hull of a cluster projected onto a plane based on the constructed basis vectors
    for fCluster in flatClusters:
        #if i == 14:
        #    break
        clusterEdges.append( [] ) # init a new cluster edge, NOTE: Note if there is an error below this list will be EMPTY
        edges2D.append( [] ) # init a new 2D edge, NOTE: Note if there is an error below this list will be EMPTY
        try: # attempt to form a convex hull from the flattened cluster, then select those cluster members at the edges and add them to 'clusterEdges'
            flatHull = sci_spatial.ConvexHull( flatClusters[i] ) # generate a convex hull of the flattened cluster
            for index in flatHull.vertices:
                clusterEdges[-1].append( clusters[i][index] ) # Add a point to the collection that represents the cluster boundary in 3D space
                edges2D[-1].append( flatClusters[i][index] ) #  Add a point to the collection that represents the cluster boundary in 2D space
        except Exception as ex: # URL, generic excpetions: http://stackoverflow.com/a/9824050
            dbgLog(1, "Encountered" , type(ex).__name__ , "on index", i , "with args:", ex.args,"with full text:",str(ex) )
            errCount += 1
        i += 1
        
    dbgLog(1, "Encountered", errCount, "errors while flattening clusters. There are ", len(clusters), "clusters.", \
           "{0:.2f}".format(errCount * 100.0 / len(clusters)) , "% error rate" )
    # 15-23 errors with the same data.  It is not the same each run!        
    # For now, try to continue with the failed clusters.  Do I need to use another convex hull implementation?
    return flatBases , flatBoxes , clusterEdges , edges2D

    
def tile_clusters_with_trial_points(edges2D, flatBoxes, flatBases): # Returns trialGraspPoints
    """ Try to tile each of the clusters with N potential grasp points, rowsN*colsN """
    dbgLog(1, "DEBUG:", "Tiling clusters..." )
    
    rowsColsN = 4 # tile each cluster bounding box with 'rowsColsN ** 2' points, only the points that lie with the full will be kept
    rowsN = 2 + rowsColsN # number of rows of test points to generate
    colsN = 2 + rowsColsN # number of cols of test points to generate
    
    index = 0 # cluster index
    trialGraspPoints = []
    for poly in edges2D:
        trialGraspPoints.append( [] ) # Add a list item even if the hull fails in order to maintain index parity
        if len(poly) > 0: # if we were actually able to construct a hull
            #         flatBoxes[index] : ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
            rowSpan = ( flatBoxes[index][1][1] - flatBoxes[index][0][1] ) / (rowsN - 1) # height of a row
            colSpan = ( flatBoxes[index][1][0] - flatBoxes[index][0][0] ) / (colsN - 1) # width of a col
            # Create a grid of test points that span the bounding box, and keep the ones within the convex hull as potential grasp points
            for i in range(1 , rowsN - 1): # for every row of test points         
                for j in range(1 , colsN - 1): # for every column of test points  
                    testPoint = ( flatBoxes[index][0][0] + colSpan * j,
                                  flatBoxes[index][0][1] + rowSpan * i )
                    if point_in_poly_w( testPoint , poly ): # the point is inside the flattened hull, create the trial grasp in 3D space on the model
                        # flatBases[i] = {'origin': xTail, 'xVec': xVec , 'yVec': yVec, 'zVec': clusterNorms[i] }
                        trialGraspPoints[-1].append( np.add( flatBases[index]['origin'],                                            
                                                             np.add( np.multiply( flatBases[index]['xVec'] , testPoint[0]  ) ,      
                                                                     np.multiply( flatBases[index]['yVec'] , testPoint[1]  ) ) ) )    
        index += 1 # increment cluster index
    return trialGraspPoints

#spaceHull = sci_spatial.ConvexHull( spamStruct['triCenters'], qhull_options='Qm' )      

def identify_stable_poses(R3PointsList, COM):
    """ Given a simplified point cloud 'R3PointsList', determine the stable poses that will support it, given COM """
    # NOTE: Taken from "pcl_test.py"
    dbgLog(1, "DEBUG:", "Identifying stable clusters..." )
    
    simpleHull = sci_spatial.ConvexHull( R3PointsList ) # construct a convex hull of the simplified model.
    # Once the qhull has constructed the hull, it will already have a list of facets which it refers to as simplices
    
    rtnPoses = [] # A list of poses, each with the required pieces of information explained below
    
    for face in simpleHull.simplices:
        
        if len(face) != 3: # Check to see if any of the hull faces have other than 3 points
            print "This face has", len(face), "points" # No faces for this hull have other than 3 points
        
        # 1. The extent of the supporting facet (coordinates of points (in order?))
        # 'face' : [319 354 172] , list of indices of 'simpleHull.points', unsure if they are in a particular order
        orderCorrect = True # First assume that the vertices are given in CCW order and then see how far you get
        
        # 2. The normal vector of the facet
        vecA = np.subtract( simpleHull.points[ face[1] ] , simpleHull.points[ face[0] ] ) # form A
        vecB = np.subtract( simpleHull.points[ face[2] ] , simpleHull.points[ face[1] ] ) # form B
        faceNorm = np_unit_vec( np.cross(vecA , vecB) ) # form the (unit) normal A cross B, this is either the normal or its opposite
        
        # Now we need to know whether this is the normal vector or its opposite. For a convex shape, the normal always
        #points away from the centroid of the hull/object.
        
        outwardVec = np.subtract( simpleHull.points[ face[0] ] , COM ) # construct a vector from the COM to this face
        
        if np.dot(faceNorm, outwardVec) < 0: # The dot product of these two will be positive if they are facing in the same direction
            faceNorm = np.multiply(faceNorm, -1) # Otherwise the normal is opposite of what it should be
            orderCorrect = False # points must be put in CCW order before they are reported
        
        # For the 2D projection, pick the projection of model's z as the plane's y
        zVec = faceNorm # z basis vector
        yVec = np_unit_vec( np_proj_vec_to_plane( [0,0,1] , zVec ) ) # y basis vector
        xVec = np.cross(yVec, zVec) # x basis vector
        # Pick the geometric center of the triangle as the origin
        triCenter = vector_mean( simpleHull.points[ face[0] ] , simpleHull.points[ face[1] ] , simpleHull.points[ face[2] ] )
        
        flatFace = [] # List of the face boundary points, expressed in a 2D plane
        
        # Make sure the points are CCW order as observed from the exterior
        if orderCorrect:
            planePoints = [simpleHull.points[ face[0] ] , simpleHull.points[ face[1] ] , simpleHull.points[ face[2] ]]
        else: 
            planePoints = [simpleHull.points[ face[0] ] , simpleHull.points[ face[2] ] , simpleHull.points[ face[1] ]]
            
        for point in planePoints:
            flatFace.append( np_basis_change_3D(point, triCenter, xVec, yVec, zVec)[:2] ) # only need the x-y coords
        
        # 3. The location of the projected COM on the facet
        projectedCOM = np_proj_vec_to_plane( np.subtract(COM , triCenter) , faceNorm) # Project the center of mass onto the the face under scrutiny   
        surfaceCOM = np_add( projectedCOM , triCenter )
        # The projected center of mass is already offset from the triangle center
        transformedCOM = np_basis_change_3D(projectedCOM, [0,0,0], xVec, yVec, zVec)[:2] # only need the x-y coords
        
        facetStable = point_in_poly_w(transformedCOM , flatFace) # is the projection of the COM within the hull face?
        
        if facetStable: # If the projection of the center of mass is within the hull face
            
            # 4. The closest distance to an edge of the facet
            shortestDist = infty
            for index, point in enumerate( flatFace ): # for every line segment in the facet, comput distance from project COM to segment
                temp = d_point_to_segment_2d( transformedCOM , elemw(index, flatFace), elemw(index+1, flatFace) )
                if temp < shortestDist: 
                    shortestDist = temp # Store the shortest distance to any segment
                    
            # Package all the the desired information as a tuple and append to the list of stable faces
            #   0. The extent of the supporting facet (coordinates of points (in order?))
            #   1. The normal vector of the facet
            #   2. The location of the projected COM on the facet
            #   3. The closest distance to an edge of the facet
            #   4. The origin and basis vectors for the facet
            
            rtnPoses.append( 
                (
                    tuple( [ simpleHull.points[ face[0] ] , simpleHull.points[ face[1] ] , simpleHull.points[ face[2] ] ]) if orderCorrect else \
                        tuple( [ simpleHull.points[ face[0] ] , simpleHull.points[ face[2] ] , simpleHull.points[ face[1] ] ] ),
                    tuple(faceNorm),
                    tuple(surfaceCOM),#tuple(projectedCOM),
                    shortestDist,
                    {
                        'origin': triCenter,         
                          'xVec': xVec,          
                          'yVec': yVec,          
                          'zVec': zVec
                    }
                )             
            )
                    
        # else the facet is not stable, do not add it to the list of stable facets
    return rtnPoses # Return list of stable poses, if any

def pose_stability(COM, mesh):
    """ Return a list of valid poses that will support the object on a flat surface with associated information """
    dbgLog(1, "DEBUG:", "Simplifying mesh for stability hull..." )
    
    allPts = all_pts_from_mesh(mesh) # store all the vertices of the STL mesh, including repeats across facets
    cloud = pcl.PointCloud() # Instantiate a point cloud data object
    cloud.from_list(allPts) # populate the point cloud object with points from the STL
    cloudSpan = span_3D( bound_box_3D( allPts ) ) # get the x,y,z extents of the point cloud
    print "Span of the model is", cloudSpan
    leaf = min(cloudSpan) * 0.1 # For now use this as the (somewhat subjective) simplification metric
    simpleFilter = cloud.make_voxel_grid_filter() # instantiate voxel grid filter based on this point cloud
    simpleFilter.set_leaf_size(leaf, leaf, leaf) # Run the voxel grid filter using the simplification metric
    simpleCloud = simpleFilter.filter() # Apply the filter and save the simplified cloud
    simplePoints = simpleCloud.to_array() # Load the simplified cloud into a list of points
    
    return identify_stable_poses(simplePoints, COM)
    
def ID_grasp_points_from_tilings(clusterNorms, trialGraspPoints, flatBases, flatEdges, flatBoxes): # approvedGraspPairs
    """ Return pairs of points (selected from trialGraspPoints) that constitute appropriate grasps """
    dbgLog(1, "DEBUG:", "Assigning grasp pairs..." )    
    
    # Entries in 'approvedGraspPairs' take the form:
        # ( ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ),  # side 0 of opposing pair
        #   ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ) ) # side 1 of opposing pair
    normalTolerance = np.pi / 30 # FIXME: This will be determined by friction cone of the gripper surface
    fingerPadDia = 0.0005 # Width of finger pad, circle where gripper friction acts # FIXME: NEED A SCALE FOR THIS!
    # TODO: Need a scale for all of the YCB models!
    opposingPairsByAngle = []
    
    # Find pairs of clusters with normals suitably close to pi radians apart
    for i, tiledClstr in enumerate(trialGraspPoints): # For each cluster plane tiled with grasp points
        if i <= len(trialGraspPoints)-2:
            for j in range(i+1,len(trialGraspPoints)): # For each cluster that follows this one
                # if the normals of the two clusters are within 'normalTolerance' of pi separation
                if abs(np_angle_between(clusterNorms[i], clusterNorms[j]) - np.pi) <= normalTolerance:
                    opposingPairsByAngle.append( (i,j) )
    
    dbgLog(1, "Found",len(opposingPairsByAngle),"pairs with close to opposite normals" )
    
    opposingPairsByProjection = []                
    # Find pairs of clusters that are opposite each other
    for pair in opposingPairsByAngle: # For each pair (i,j), where i,j are cluster indices
        # Project the j bounding box onto the i plane
        boxesIntersect = False
        i = pair[0]
        j = pair[1]
        Ii_flatbox = flatBoxes[i] # ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
        I_bb = [ Ii_flatbox[0] , [ Ii_flatbox[1][0] , Ii_flatbox[0][1] ] , Ii_flatbox[1] , [ Ii_flatbox[0][0] , Ii_flatbox[1][1] ] ] # The i bounding box in the i basis
        Jj_flatbox = flatBoxes[j] # ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
        
        J_spaceBox = [ # This is the j cluster bounding box expressed in the model coordinate frame
            np.add( flatBases[j]['origin'], # TODO: This calculation appears so frequently it should be a function!                                     
                    np.add( np.multiply( flatBases[j]['xVec'] , Jj_flatbox[0][0] ) ,      
                            np.multiply( flatBases[j]['yVec'] , Jj_flatbox[0][1] ) ) ),
            np.add( flatBases[j]['origin'], 
                    np.add( np.multiply( flatBases[j]['xVec'] , Jj_flatbox[1][0] ) ,      
                            np.multiply( flatBases[j]['yVec'] , Jj_flatbox[0][1] ) ) ),
            np.add( flatBases[j]['origin'], 
                    np.add( np.multiply( flatBases[j]['xVec'] , Jj_flatbox[1][0] ) ,      
                            np.multiply( flatBases[j]['yVec'] , Jj_flatbox[1][1] ) ) ),
            np.add( flatBases[j]['origin'], 
                    np.add( np.multiply( flatBases[j]['xVec'] , Jj_flatbox[0][0] ) ,      
                            np.multiply( flatBases[j]['yVec'] , Jj_flatbox[1][1] ) ) )
        ]
        
        J_bb = [ # The j bounding box projected onto the i X-Y plane
            [ # TODO: This calculation appears so frequently it should be a function!
                np_vec_proj(np.subtract(J_spaceBox[0] , flatBases[i]['origin']), flatBases[i]['xVec']),
                np_vec_proj(np.subtract(J_spaceBox[0] , flatBases[i]['origin']), flatBases[i]['yVec'])
            ],
            [ 
                np_vec_proj(np.subtract(J_spaceBox[1] , flatBases[i]['origin']), flatBases[i]['xVec']),
                np_vec_proj(np.subtract(J_spaceBox[1] , flatBases[i]['origin']), flatBases[i]['yVec'])
            ],
            [ 
                np_vec_proj(np.subtract(J_spaceBox[2] , flatBases[i]['origin']), flatBases[i]['xVec']),
                np_vec_proj(np.subtract(J_spaceBox[2] , flatBases[i]['origin']), flatBases[i]['yVec'])
            ],
            [ 
                np_vec_proj(np.subtract(J_spaceBox[3] , flatBases[i]['origin']), flatBases[i]['xVec']),
                np_vec_proj(np.subtract(J_spaceBox[3] , flatBases[i]['origin']), flatBases[i]['yVec'])
            ]
        ] # 'I_bb' and 'J_bb' are now expressed in the same basis
        # check if the i box and the j box overlap in any way in the i plane
        # If one box contains the other, then every point of the contained lies within the container, test one of each
        boxesIntersect = point_in_poly_w(I_bb[0] , J_bb) # Test if I_bb contained in J_bb
        if not boxesIntersect:
            boxesIntersect = point_in_poly_w(J_bb[0] , I_bb) # Test if J_bb contained in I_bb
        # If one box intersects the other, but is not contained by the other, then at least one line segment intersects
        if not boxesIntersect:
            for iDex, iPnt in enumerate(I_bb):
                for jDex, jPnt in enumerate(J_bb):
                    boxesIntersect = intersect_seg_2D( [ iPnt , elemw(iDex+1,I_bb) ] , [ jPnt , elemw(jDex+1,J_bb) ] )
                    if boxesIntersect:
                        break
                if boxesIntersect:
                    break
        # Candidate grasp points may still be valid if they are within a finger-pad-width of each other
        if not boxesIntersect:
            for iDex, iPnt in enumerate(I_bb):
                for jDex, jPnt in enumerate(J_bb): # TODO: Merge the contents of this loop with the one above
                    # Test if the two points are close to each other
                    boxesIntersect = sci_spatial.distance.euclidean(iPnt , jPnt) <= fingerPadDia
                    # Test if the two points are close the associated edges of the bounding boxes
                    if not boxesIntersect:
                        boxesIntersect = d_point_to_segment_2d( iPnt , jPnt , elemw(jDex+1,J_bb) ) <= fingerPadDia
                    if not boxesIntersect:
                        boxesIntersect = d_point_to_segment_2d( jPnt , iPnt , elemw(iDex+1,I_bb) ) <= fingerPadDia
                    if boxesIntersect:
                        break
                if boxesIntersect:
                    break
                
        # if they do, add the pair to a list of pairs that are close to opposite
        if boxesIntersect:
            opposingPairsByProjection.append( pair )
    
    dbgLog(1, "Found",len(opposingPairsByProjection),"opposite-normal pairs with favorable projections")
    
    # Entries in 'approvedGraspPairs' take the form:
        # ( ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ),  # side 0 of opposing pair
        #   ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ) ) # side 1 of opposing pair
    
    approvedGraspPairs = [] # List of all grasp point pairs that meet criteria
    
    for n, pair in enumerate(opposingPairsByProjection): # for each pair of suitable clusters
        # JUST GOING TO BUILD PAIRS FROM THE CENTERS OF CLUSTERS UNTIL I CAN FIGURE OUT WHAT IS GOING ON
        i = pair[0]
        j = pair[1]
        for pairDex, clusDex in enumerate(pair): # for each index in the suitable pair
            area , centroid = poly2D_area_centroid( flatEdges[clusDex] ) # Calc centroid
            centroid3D = np.add( flatBases[clusDex]['origin'], # Transform centroid to STL basis
                                 np.add( np.multiply( flatBases[clusDex]['xVec'] , centroid[0] ) ,      
                                         np.multiply( flatBases[clusDex]['yVec'] , centroid[1] ) ) )
            trialGraspPoints[clusDex].append( centroid3D ) # Append centroid to the tiling
        # Arriving at this point, we have a pair of clusters that have been evaluated for the degree to which they are
        #parallel and whether they lie opposite to each other in projection. Construct tuples of
        # ( (index of cluster 1 , index of centroid in cluster 1's grasp candidates) , 
        #   (index of cluster 2 , index of centroid in cluster 2's grasp candidates) )
        approvedGraspPairs.append( ( ( i , len(trialGraspPoints[i])-1 ) , ( j , len(trialGraspPoints[j])-1 ) ) )
        
    return approvedGraspPairs


# == Grasp Point Generation Demo ==

 


graspPairsForPose = [] # List of lists associated with 'stableClusters' that denotes the graspable pairs in that pose
handPoses = [] # NOTE: Do not keep this in the production code, it is for debugging only

def np_xform_pnt_to_3Dbasis(origin, bases, point):
    """ Express a 'point' in some 'bases' with 'origin' """
    return np.add( origin, # the grasp point                                
                   np_add( np.multiply( bases[0] , point[0] ) ,   # The shifted hand coordinates expressed in a
                           np.multiply( bases[1] , point[1] ) ,   # basis with the X-Y plane coplanar with the grasp plane
                           np.multiply( bases[2] , point[2] ) ) )

"""
NOTE: For now avoiding any sort of collision detection whatsover in this phase. Collisions will only be evaluated after an edge of the graph 
      is chosen. Then the grasp is evaluated for feasibility with respect to IK and collisions.

TODO: There is probably a much faster way of doing this, likely solution is far fewer points in the hand model, try Meshlab"""

def prune_grasps_for_poses( poseStability , approvedGraspPairs , trialGraspPoints , flatBases , handPoints ):
    """ Now that all the models have been processed for canditate poses and grasp points, Identify which points are allowed for each pose """
   
    #PKLpath = 'output/PlayGoRainStakCups2Oran.pkl' # load a processed mesh for this test
    #procMesh = load_processed_mesh(PKLpath) # load a processed mesh
    
    #dbgLog(1, "DEBUG: Load success!", PKLpath )          
    dbgLog(1, 'This file contains', len(approvedGraspPairs), 'approved grasp pairs, as follows', approvedGraspPairs )
    
    #poseStability = procMesh['clusterStability'] # fetch list of pose stability information
    dbgLog(1,"There are",len(poseStability),"approved supporting faces") # 61 supporting facets
    #plot_opposing_grasp_pnts(procMesh, divisor = 2)
       
    indexFingTip = [ -0.079375 , -0.0428625 , 0.06 ] # This is the tip of the index finger in "simple-hand-closed_m.stl"
    
    #for index, stblClusIndex in enumerate(stableClusters): 
    
    nonCollidingGrasps = [] # Holds lists of grasp pairs that will not collide with the table per stable pose
    
    for index, stablePose in enumerate(poseStability): # For each face that will support the model
    
        goodGraspPairs = [] # noncolliding grasp pairs for this pose

        #index = random.randrange( len(poseStability) ) # Choose a stable pose at random 
        #stablePose = poseStability[index] # Store info about the stable pose
        
        # Get information about the supporting face of the hull    
        sprtBase = stablePose[4] #procMesh['flatBases'][stblClusIndex] # The basis for the supporting cluster
        sprtOrgn = sprtBase['origin'] # The origin of the supporting face
        sprtNorm = np_unit_vec( sprtBase['zVec'] ) # The normal of the supporting face
        
        for pairDex , pair in enumerate(approvedGraspPairs): #enumerate(procMesh['approvedGraspPairs']): # for each of the approved grasp pairs in the entire model
            #pairDex = random.randrange( len(procMesh['approvedGraspPairs']) ) # choose a random approved grasp pair
            #pair = procMesh['approvedGraspPairs'][pairDex] # store the addresses of the grasp pair
            
            projectedHand = [] # The collection of hand points projected onto the supporting normal     
            graspCollides = False # Does the proposed grasp collide with the supporting surface in this pose?
            
            for addr in pair: # for each of the two points in the pair
                #smallCount = 0 # not sure what this was for?
                # consider the first of the two grasp points
                clusDex = addr[0] # The cluster associated with this point
                clusBase = flatBases[clusDex] #procMesh['flatBases'][clusDex] # The basis associated with this cluster
                
                #point = item_at_addr( procMesh['trialGraspPoints'] , addr ) # the point under strutiny
                point = item_at_addr( trialGraspPoints , addr ) # the point under strutiny
                # 2. For every selected grasp point, Orient the hand in some likely configuration
                zHand = np_unit_vec( clusBase['zVec'] ) # Normal of the grasp plane is the Z basis vector for the hand model, unit vector
                # 2.a. Project the opposite of the supporting normal onto the cluster of the point under scrutiny (grasp plane)
                # X axis is the projection of the opposite of the supporting normal onto the grasp plane, unit vector
                xHand = np_unit_vec( np_proj_vec_to_plane( np.multiply( sprtBase['zVec'] , -1 ) , zHand) )
                yHand = np_unit_vec( np.cross( zHand , xHand ) )# Use the cross product to find the third basis vector # Cross of two unit vectors is unit
                #    - AND -
                
                #    Place the middle of the finger on the grasp point
                transformedHand = [] # list of points of the hand, with the middle of the finger on the grasp point and oriented as above
                for hPoint in handPoints: # For every point in the list of hand points
                    offsetPoint = np.subtract( hPoint , indexFingTip ) # Shift the entire hand model so that the fingertip is at the new origin
                    # The shifted hand coordinates expressed in a basis with the X-Y plane coplanar with the grasp plane
                    transformedHand.append( np_xform_pnt_to_3Dbasis(point, [xHand,yHand,zHand], offsetPoint) )
        
                handPoses.append( transformedHand ) # NOTE: Storing each proposed hand pose for debugging purposes ONLY
                #print "Found", smallCount, "tiny Z values for this hand pose" # "Found 86 tiny Z values for this hand pose"
                # 3. Check if the hand model collides with the supporting surface, Collision checking should be very easy:
                
                for xfHndPnt in transformedHand:
                    # 3.a. subtract the hand point from the face origin,
                    offsetHndPnt = np.subtract( xfHndPnt , sprtOrgn )
                    # 3.b. then project the point onto the cluster normal.
                    prjctdScalar = np_vec_proj( offsetHndPnt , sprtNorm )
                    if prjctdScalar > 0:
                        # 3.b.a. If the result is positive, it is above the supporting cluster and is thus inside the ground
                        graspCollides = True
                    # 3.b.b. else result is negative, it is behind the supporting cluster and is thus above the ground
                        
                    # 3.c. Store this projected point for display
                    projectedHand.append( np_xform_pnt_to_3Dbasis(sprtOrgn, [sprtBase['xVec'],sprtBase['yVec'],sprtBase['zVec']], [0,0,prjctdScalar]) )
            
            if graspCollides:
                pass
                # dbgLog(1, "Grasp collides!" )
            else:
                # dbgLog(1, "Grasp does not impact table!" )
                goodGraspPairs.append( pair ) # If neither of the two points in the pair produce a collision, add the pair to list of valid ones for this pose
                
        nonCollidingGrasps.append( goodGraspPairs ) # Store all the good grasp pairs for this pose
        
    return nonCollidingGrasps
        
#    clusterEdges = procMesh['clusterEdges']
#    approvedGraspPairs = procMesh['approvedGraspPairs']
#    trialGraspPoints = procMesh['trialGraspPoints']
#    
#    fig2 = plt.figure()
#    ax2 = fig2.add_subplot(111, projection='3d')
#    
#    basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
#    numColors = len(basicColors)
#    
#    divisor = 191
#    scale = 0.02
#    
#    for i, edge in enumerate(clusterEdges):
#        if len(edge) > 0:
#            xs,ys,zs = split_to_comp_cycle( edge )
#            #ax2.scatter(xs, ys, zs, c='k', marker='o', s=7) # basicColors[i%(numColors-1)]
#            ax2.plot(xs, ys, zs, c='grey') # basicColors[i%(numColors-1)]
#    
##    for n, pair in enumerate(approvedGraspPairs):
##        if n % divisor == 0:
#    # Construct a unit vector between the origins of the two clusters
#    n = 4
#    originI = item_at_addr(trialGraspPoints, pair[0])
#    originJ = item_at_addr(trialGraspPoints, pair[1])
#    pointAwayI = np_unit_vec( np.subtract( originI , originJ ) )
#    pointAwayJ = np_unit_vec( np.subtract( originJ , originI ) )
#    pointI = np.add( originI , np.multiply( pointAwayI , scale ) )
#    pointJ = np.add( originJ , np.multiply( pointAwayJ , scale ) )
#    xs,ys,zs = split_to_components( (originI,pointI) )
#    ax2.plot(xs, ys, zs, c=basicColors[n%(numColors-1)]) # 
#    xs,ys,zs = split_to_components( (originJ,pointJ) )
#    ax2.plot(xs, ys, zs, c=basicColors[n%(numColors-1)]) # 
#    xs,ys,zs = split_to_components( (originI,originJ) )
#    ax2.plot(xs, ys, zs, c=basicColors[n%(numColors-1)], ls='-.') # '--' ':'
#            
#    xs, ys, zs = split_to_components(handPoses[0])
#    ax2.plot(xs, ys, zs, c='black')#, #ls='-.')  
#    
#    xs,ys,zs = split_to_components( [ sprtBase['origin'] ] )
#    ax2.scatter(xs, ys, zs, c='red', marker='+' , s=300)
#    
#    xs,ys,zs = split_to_components( projectedHand )
#    ax2.scatter(xs, ys, zs, c='purple', s= 12)
#    
#    normLen = 0.025
#    normVec = np_xform_pnt_to_3Dbasis( sprtBase['origin'] , [sprtBase['xVec'] , sprtBase['yVec'] , sprtBase['zVec']], [0 , 0 , normLen] )
#    xs,ys,zs = split_to_components( [ sprtBase['origin'] , normVec ] )  
#    ax2.plot(xs, ys, zs, c='red')
#    
#    plot_bases_3D_mpl(ax2, [0,0,0], xHand, yHand, zHand, 0.02)
#            
#    plt.gca().set_aspect('equal')    
#    plt.show()


# now plot the hand superimposed on the model to examine the results


# len(handPoses) = stableClusters * approvedGraspPairs = 2610
            
       
# there needs to be a list of grasp pairs for each stable pose       
  
# == Model Batch Processing ==  
  
plt.close('all') # clear any figures we may have created before 

def load_one_file(path, targetDir, handPoints, plot = False):
    """ Load one file and perform clustering / grasp processing, """
    fileStr = os.path.split(path)[1][:-4]
    #Stopwatch.start()
    #spamStruct = load_and_cluster_STL('spam_simplified.stl') 
    spamStruct = load_and_cluster_STL(path, handPoints)
    #Stopwatch.stop()
    #dbgLog(1, "Loaded",path,"in", Stopwatch.elapsed() )# Loaded Spam in 1.08045860064
    dbgLog(1, "Created",len(spamStruct['clusters']) )
    dbgLog(1, "Found centroid:",spamStruct['volumeCentroid'] )
    
    COMwasInside = len(spamStruct['clusterStability'])
#    for item in spamStruct['clusterStability']: # Clusters are no longer the basis for pose stability
#        if item and item['projCOMinside']:
#            COMwasInside += 1
    dbgLog(1, "Found",COMwasInside,"potential suitable poses." )
    if plot:
        #plot_clusters( spamStruct['clusters'] )
        #plot_clusters_with_tiling( spamStruct['clusterEdges'] , spamStruct['trialGraspPoints'] )
        plot_opposing_grasp_pnts(spamStruct, scale = 0.02, divisor = 1)
    fName = targetDir + fileStr + '.pkl'
    try:
        f = open( fName , 'wb') # open a file for binary writing to receive pickled data
        cPickle.dump(spamStruct, f) # changed: pickle.dump --> cPickle.dump
        f.close()
    except Exception as err:
        dbgLog(1, "FILE ERROR:", "Could not pickle file", fName, str(err) ) 
    return fName
    
# load_one_file('pssn_stl/2inMetaWash.stl','outout/')

def pckl_all_models(stlDir, targetDir, plot = False):
    """ Process all STL models in the 'stlDir' directory and save the 'cPickle'd (serialized) structures in the 'targetDir' """
    dbgLog(1, "DEBUG:", "Batch process of all STL models began at", nowTimeStamp() )    
    
    # ~ init ~
    stlList = os.listdir(stlDir)
    loopCount = 0
    failCount = 0
    goodCount = 0
    failNames = []
    # ~ Open Log file ~
    outFile = open(targetDir + 'log_' + nowTimeStamp() + '.txt', 'w') # Open a file for logging
    outFile.write( "Starting at " + nowTimeStamp() + endl)
    outFile.write( endl )
    
    # ~~ Preliminary Work ~~
    # Fetch hand info
    # NOTE: The rest of the arm/fingers are not considered and should be part of the feasibility evaluation during graph search
    # 1. Load the hand model
    dbgLog(1, "DEBUG:", "Loading hand file...")    
    handSTLPath = "simple-hand-closed_m.stl" # A simple representation of the hand, built in and exported from FreeCAD
    #outPointsPath = "output/simple-hand-closed_m.stl" 
    pklPointsPath = "output/simple-hand-closed_m_uniq-pts.pkl" # Pickle file of all the unique vertices in the hand STL
    
    if not os.path.isfile(pklPointsPath): # if a points-only file of the hand has not already been generated, then generate it
        #hand_mesh = mesh.Mesh.from_file(handSTLPath) 
        uniq_pts_from_stl(handSTLPath, pklToFile = True) # Load the hand STL file and pickle as a points-only file
    
    handPoints = load_pkl_struct(pklPointsPath) # load the generated file
    dbgLog(1, "DEBUG:", "Hand file loaded!")
    
    # PICKLE ALL THE THINGS
    for model in stlList:
            
        loopCount += 1
        dbgLog(1, "DEBUG:", "Processing model",loopCount,"of",len(stlList))
        Stopwatch.start()
        
        fName = os.path.join(stlDir , model)
        outFile.write("Attempting " + fName + endl)
        try:
            written = load_one_file(fName, targetDir, handPoints, plot) # try to load the file
            outFile.write("\t" + fName + " - *SUCCESS* -->" + written + endl)
            goodCount += 1
        except BaseException as err:
            ex_type, ex, tb = sys.exc_info()
            stackStr = ''
            for item in traceback.format_tb(tb):
                stackStr += item
            
            outFile.write("\t" + fName + " - !FAILURE!" + endl)
            outFile.write("\t" + str(err) + endl + "\t" + stackStr + endl)
            
            failCount += 1
            failNames.append( fName ) # Store the failed filenames for later investigation
            break # for now break on the first failure
    
        Stopwatch.stop()
        dbgLog(1, "DEBUG:", "Processed model",loopCount,"of",len(stlList),",",Stopwatch.elapsed(),"elapsed.",endl)
    
    # Processing Report
    outFile.write( "Success Rate: " + str( (goodCount * 1.0 / loopCount) * 100 ) + "%" + endl)
    outFile.write( "Failure Rate: " + str( (failCount * 1.0 / loopCount) * 100 ) + "%" + endl)
    outFile.write( endl )
    if len(failNames): # If any files failed to load, list them
        outFile.write( "=== Failed Files ===" + endl ) # List all the files that threw an error
        for name in failNames:
            outFile.write( name + endl )
        outFile.write( "=== End Failed ===" + endl)
        outFile.write( endl )
    outFile.write( "Ending at " + nowTimeStamp() + endl + endl)
    if dbg_data_exists(): # If there is text in the debug string, dump to file
        outFile.write( endl )
        outFile.write( "=== Debug Messages ===" + endl )
        outFile.write( dbg_contents() + endl )
        outFile.write( "=== End Debug ===" + endl )
        outFile.write( endl )
    outFile.close() # Always close the file!
    dbgClr() # Clear the debug string

#pckl_all_models( 'pssn_stl/' , 'output/' ) # uncomment to cluster and pickle all poisson STL models in a batch process

def load_processed_mesh(pklFilePath):
    """ Load a pickled mesh object, and hope for the best """
    f = open( pklFilePath , 'rb')
    rtnStruct = cPickle.load(f)
    f.close()
    return rtnStruct
  
def pkl_candidate_grasps(pklDir, targetDir, overwrite = False):
    """ Process all STL models in the 'stlDir' directory and save the 'cPickle'd (serialized) structures in the 'targetDir' """
    dbgLog(1, "DEBUG:", "Batch process of all STL models began at", nowTimeStamp() )    
    
    # ~ init ~
    pklList = os.listdir(pklDir)
    loopCount = 0
    failCount = 0
    goodCount = 0
    failNames = []
    
    # ~ Open Log file ~
    outFile = open(targetDir + 'log_stable_grasps' + nowTimeStamp() + '.txt', 'w') # Open a file for logging
    outFile.write( "Starting at " + nowTimeStamp() + endl)
    outFile.write( endl )
    
    # ~~ Preliminary Work ~~
    # Fetch hand info
    # NOTE: The rest of the arm/fingers are not considered and should be part of the feasibility evaluation during graph search
    # 1. Load the hand model
    dbgLog(1, "DEBUG:", "Loading hand file...")    
    handSTLPath = "simple-hand-closed_m.stl" # A simple representation of the hand, built in and exported from FreeCAD
    #outPointsPath = "output/simple-hand-closed_m.stl" 
    pklPointsPath = "output/simple-hand-closed_m_uniq-pts.pkl" # Pickle file of all the unique vertices in the hand STL
    
    if not os.path.isfile(pklPointsPath): # if a points-only file of the hand has not already been generated, then generate it
        #hand_mesh = mesh.Mesh.from_file(handSTLPath) 
        uniq_pts_from_stl(handSTLPath, pklToFile = True) # Load the hand STL file and pickle as a points-only file
    
    handPoints = load_pkl_struct(pklPointsPath) # load the generated file
    dbgLog(1, "DEBUG:", "Hand file loaded!")
    
    # build a list of the PKLs that were produced from the first pass
    meshList = [ pkl for pkl in pklList if ( (not 'grasp' in pkl.lower() ) and                         # Not a PKL that we labeled as a grasp listing
                                             (not 'simple-hand-closed_m_uniq-pts' in pkl.lower() ) and # Not the hand model PKL 
                                             ( pkl[-4:] == '.pkl' ) ) ]                                # Is a PKL file
    
    dbgLog(1, "DEBUG:", "There are", len(meshList) , "matching the criteria")
    
    for meshName in meshList: # For each of the the first-pass structure filenames
            
            loopCount += 1
            dbgLog(1, "DEBUG:", "Processing model",loopCount,"of",len(meshList))
            Stopwatch.start()
            
            fName = os.path.join(pklDir , meshName) # construct a path to the stuct PKL
            outFile.write("Attempting " + fName + endl)
            
            try:
                pklPath = targetDir + meshName[:-4] + '_stable-grasps.pkl' # Give the file the same name, but with a PKL extension
                                
                if (not os.path.exists(pklPath)) or overwrite: # if the target file does not exist or we are overwriting it
                    mesh = load_processed_mesh(fName) # try to load the file
                    # ID candidate grasps per pose
                    graspsForPoses = prune_grasps_for_poses( mesh['clusterStability'] , mesh['approvedGraspPairs'] , mesh['trialGraspPoints'] , 
                                                             mesh['flatBases'] , handPoints ) 
                    
                    f = open( pklPath , 'wb' ) # open a file for binary writing to receive pickled data
                    cPickle.dump( graspsForPoses , f ) # changed: pickle.dump --> cPickle.dump
                    f.close()
                    dbgLog(1, "DEBUG:", "PKL was cPickled to a list of noncolliding grasps per stable pose in a file:", pklPath)
                    outFile.write("\t" + fName + " - *SUCCESS* -->" + pklPath + endl)
                else: # else file exists and we do not wish to overwrite
                    dbgLog(1, "DEBUG:", "Target output file exists:", pklPath)
                    outFile.write("\t" + fName + " - *NO ACTION* -->" + pklPath + endl)
                    
                goodCount += 1
            except BaseException as err: # Something bad happened, generate and log a report
                ex_type, ex, tb = sys.exc_info() # fetch info for last exception
                stackStr = ''
                for item in traceback.format_tb(tb): # Iterate over stack trace and print
                    stackStr += item
                
                outFile.write("\t" + fName + " - !FAILURE!" + endl)
                outFile.write("\t" + str(err) + endl + "\t" + stackStr + endl)
                
                failCount += 1
                failNames.append( fName ) # Store the failed filenames for later investigation
                break # for now break on the first failure
        
            Stopwatch.stop()
            dbgLog(1, "DEBUG:", "Processed model",loopCount,"of",len(meshList),",",Stopwatch.elapsed(),"elapsed.",endl)
    
    # Processing Report
    outFile.write( "Success Rate: " + str( (goodCount * 1.0 / loopCount) * 100 ) + "%" + endl)
    outFile.write( "Failure Rate: " + str( (failCount * 1.0 / loopCount) * 100 ) + "%" + endl)
    outFile.write( endl )
    if len(failNames): # If any files failed to load, list them
        outFile.write( "=== Failed Files ===" + endl ) # List all the files that threw an error
        for name in failNames:
            outFile.write( name + endl )
        outFile.write( "=== End Failed ===" + endl)
        outFile.write( endl )
    outFile.write( "Ending at " + nowTimeStamp() + endl + endl)
    if dbg_data_exists(): # If there is text in the debug string, dump to file
        outFile.write( endl )
        outFile.write( "=== Debug Messages ===" + endl )
        outFile.write( dbg_contents() + endl )
        outFile.write( "=== End Debug ===" + endl )
        outFile.write( endl )
    outFile.close() # Always close the file!
    dbgClr() # Clear the debug string  
  
#pkl_candidate_grasps('output/', 'grasps/') # Uncomment to generate noncolliding grasp pairs per stable pose


# NOTE : It seems like cPickle will keep track of object references?
# https://docs.python.org/3/library/pickle.html#relationship-to-other-python-modules

def fully_connected_graph_from_struct( pklPath , graspPath ): 
    """ Generate a graph from all stable poses and graphs using stored cluster and pose data, store in a new file if requested """
    # 1. Obtain list of stable poses from mesh
    mesh = load_processed_mesh(pklPath) # try to load the file
    stablePoses = mesh['clusterStability']
    # 2. Obtain per-pose lists of noncolliding grasps for the same object
    graspLists = load_processed_mesh(graspPath)
    
    superGraph = [] # A list of subgraphs, one for eachs stable pose
    # 3. For each stable pose of this object
    for poseDex , pose in enumerate(stablePoses):
        # 3.a. Create a subgraph for each pose
        currGraph = Graph() # # For now allow the subgraph to be identified by its tag
        # 3.b. Create nodes for all the noncolliding grasp pairs associated with the pose
        for pair in graspLists[poseDex]:
            currGraph.add_node_with_alias( pair ) # Node alias should be the grasp pair tuple
        # 3.c. Fully connect all the grasps for this pose with transfer edges
        allNodes = currGraph.nodes.item_list() # Fetch a list of all the items in the lookup
        for noDex , node in enumerate(allNodes): # For each node in the subgraph
            for followDex in range( noDex + 1 , len(allNodes) ): # for all the following nodes in the list
                # Connect the grasps in this pose with undirected edges
                currGraph.connect_edge( allNodes[noDex] , allNodes[followDex] , weight = 'transfer' ) # transfers are between grasps
        superGraph.append( currGraph )
        
    # 3.d. Connect common grasps between placements with transition edges
    transitions = set([]) # The set of transfers that have already been looked at
    
    for graphDex , subgraph in enumerate(superGraph): # for each subgraph in this layer
        allNodes = subgraph.nodes.item_list() # produce a list of nodes for this subgraph
        for node in allNodes: # for each node in the subgraph
            if not node.alias in transitions: # if this grasp has not already been connected
                for innerDex , sgraph in enumerate(superGraph): # for each subgraph in this layer
                    if innerDex != graphDex: # transfers are not between grasps on the same pose
                        # find out if the other subgraph has a Node with the same alias (same grasp)
                        otherNode = sgraph.nodes.get_by_als(node.alias) # if DNE returns None
                        if otherNode: # if a Node object was returned
                            connect_edge_across_graphs( node , otherNode , weight = 'transition' ) # Transitions are between poses/subgraphs
            transitions.add( node.alias ) # Transisitons between grasps of this type now exist between all applicable subgraphs, do not check again
            
    return superGraph # Return the fully connected initial supergraph

#set_dbg_lvl(3)

# def uniformCostSearch( superGraph , startNode , goalTag ): # We don't need access to the toplevel graph container
# TODO: Create a "SuperGraph" TagObj so that finding a subgraph is less tedious!
def uniformCostSearch( startNode , goalTag ):
    """Search the node of least total cost first."""
    
    #def UCS_helper(superGraph, node):
    def UCS_helper(node):
        """ Does the work of Uniform Cost Search """
        
        #   Each item in frontier [node, [PATHTONODE]]
        UCS_helper.frontier.push( [node, []] , 0 ) # push the root node onto the frontier with an empty path
        #                         item       , value
        
        while not UCS_helper.frontier.isEmpty(): # while there are still nodes on the frontier queue
            temp = UCS_helper.frontier.pop() # get the first item in the queue
            while temp[0].tag in UCS_helper.popped: # if this node has been popped before, it was at a lower cost
                temp = UCS_helper.frontier.pop() # get the next item in the queue
            # popped a node that has not been popped before
            UCS_helper.popped.add( temp[0].tag ) # add to set of popped nodes
            parent = temp # good next node, store it
            UCS_helper.visited.add( parent[0].tag ) # mark this item as visited
            #if problem.isGoalState( parent[0] ): # if Agent has arrived at the goal, then
            if parent[0].graph.tag == goalTag: # if Agent has arrived at the goal, then
                return parent[1] # return the path that got it there
            #neighbors = prob.getSuccessors( parent[0] ) # find all the successors of the current node
            neighbors = parent[0].get_successors() # find all the successors of the current node
            dbgLog(3,"Found",len(neighbors),"successors")
            for edge in neighbors: # for each successor of this node, do
                # if the successor has not been visited and is also not presently in the frontier queue
                edgeTarget = edge.othr_obj(parent[0])
                if not edgeTarget.tag in UCS_helper.visited: #and nghbr[0] in UCS_helper.heaped:
                #not is_in_path_pri_queue(UCS_helper.frontier, nghbr[0]):
                    pathTo = parent[1][:] # copy the path to the parent
                    pathTo.append( 
                        # transition/transfer , Node.tag: BGN-->END ,              Graph.tag: BGN-->END                         , target ref , target graph ref
                        ( edge.weight ,         (parent[0].tag , edgeTarget.tag) , (parent[0].graph.tag , edgeTarget.graph.tag) , edgeTarget , edgeTarget.graph ) 
                    ) # append the direction to this node
                    UCS_helper.frontier.push( [ edgeTarget , pathTo ] , 1 ) # push the successor onto the frontier queue
        return [False] # search expanded all nodes without result
    
    #startNode = problem.getStartState() # fetch the starting node    
    
    # ~ init Functor ~
    UCS_helper.frontier = util.PriorityQueue() # using a priority queue this time (From UC Berkeley, "util.py")
    UCS_helper.visited = set([]) # the set of visited nodes
    UCS_helper.popped = set([]) # a set of popped nodes
    
    #return UCS_helper(problem, startNode) # invoke the search on the starting node
    return UCS_helper(startNode) # invoke the search on the starting node

pklPath = 'output/PlayGoRainStakCups1Yell.pkl'
graspPath = 'grasps/PlayGoRainStakCups1Yell_stable-grasps.pkl'

def gen_and_pkl_all_graphs( pPklPath , pGraspPath , targetDir ):
    """  """
    pass 

if True: # Try out the graph generation and graph search
    # Generate the graph and get an idea of how connected it is
    
    bigGraph = fully_connected_graph_from_struct( pklPath , graspPath )
    print "Found",len(bigGraph),"supporting poses"
    print "Number of items in each subgraph"
    subHash = hash_TagObj_list(bigGraph)
    for sub in bigGraph:
        print sub.tag ,
    
    # Take a look at one of the nodes
    if False:
        print endl
        trialNode = bigGraph[0].nodes.item_list()[0]
        print trialNode.tag, trialNode.alias, "has",len(trialNode.edges),"edges"
        for edge in trialNode.edges:
            print edge.weight,",",edge.othr_obj(trialNode).tag, ",", edge.othr_obj(trialNode).graph.tag
    
    # Attempt a graph search
    if True:
        # 1. Pick an origin 
        start = random.choice( bigGraph ) 
        startGrasp = random.choice( start.nodes.item_list() ) # Choose a random starting grasp
        print endl
        sep("Simple Graph Search")
        print "Starting at",start.tag
        # 2. Pick a destination
        goal = random.choice( bigGraph )
        while goal.tag == start.tag: # If the start and goal are the same, pick a new one until they are different
            goal = random.choice( bigGraph )
        print "Goal is", goal.tag
        # 3. Find a path between
        graspPlan = uniformCostSearch( startGrasp , goal.tag )
        for action in graspPlan:
            print action


    """ vv- SYMPTOMS -vv
    * The last transition is never displayed
      IDEAS:
        - Somehow need one last "destination-only" draw iteration 
    * Graph search almost always ends with two transitions, even though the shortest path should never have two transitions end-to-end. Transitions
      must always share a grasp and therefore the shortest path should be between grasp X at the beginning of Transition 1 and the grasp X at the end
      of Transition 2.
      IDEAS:
        - Graph not built correctly?
        - UCS does not find optimal path because the cost of every move is identical? UCS degenerates into DFS when all costs identical.  At each
          iteration the first node pushed on the last iteration will be the first popped by "util.py".  Search stops at first encounter with goal.
    """
    # Visualize the results of the search        
    if True:
        
        # TODO: Add Node/Edge labels to the visualization?        

        flatCenter = [0,0] # Point on the X-Y plane that the subgraphs will be centered on
        vertSpacing = 10 #5 # Vertical spacing for transitions
        circDiam = 20 # display diameter of subgraph
        specialColor = 'green'
        plainColor = 'grey'
        # 1. Fetch the first subgraph
        currSub = start
        currNodes = start.nodes.item_list() # Get all the nodes so that we can iterate through
        nodeHash = hash_TagObj_list(currNodes) # Relate tags to indices so that we can connect them correctly
        # Generate points around a circle
        numPts = len(currNodes)
        subPts = circ_spacing( circDiam , numPts, flatCenter) # Get the node locations for this subgraph
        # 2. Paint initial node special, store special
        specialNodes = [] # A list of indices that are painted a special color
        specialEdges = [] # A list of edges that are painted a special color
          
        # Init graphics          
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        basicColors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
        numColors = len(basicColors)
        
        #specialNodes.append( find_tag_in_node_list( currNodes , startGrasp.tag ) ) # store the index of the special
        ptFromLast = None # Point on the last subgraph we are transitioning from     
        lastAction = None
        
        zDex = 0
        
        #graspPlan.append( ('transfer', graspPlan[-1][1], graspPlan[-1][2], graspPlan[-1][3], graspPlan[-1][4] ) )
             
        # 3. for each action in the plan
        for level , action in enumerate(graspPlan):
            nodesR3 = []
            specialR3 = []
            allR3 = []
            
            transfers = set([]) # transfer edges for this subgraph
            edgePairListR3 = []
            edgePairListSpecialR3 = []
            specialNodes.append( find_tag_in_node_list( currNodes , action[1][0] ) ) # store the index of the special

#            if ptFromLast:
#                zDex += 1
#                print "Inrement zDex to", zDex
            
            #zChrd = level * vertSpacing            
            zChrd = zDex * vertSpacing            
            
            # 3.a. Determine if the action is a transition / transfer
            # 3.b. If it is a transfer, paint destination node special, paint edge special, store special edge and node
            if action[0] == 'transfer':
                specialNodes.append( find_tag_in_node_list( currNodes , action[1][1] ) ) # store the index of the special
                specialEdges.append( action[1] )
            # 3.c. Paint the non-special nodes plain , Paint the non-special edges plain
            
            for noDex , node in enumerate(currNodes): # for each of the nodes in this subgraph
                # ~~ Nodes ~~
                point = (subPts[noDex][0] , subPts[noDex][1] , zChrd) # generate a point in 3D space
                allR3.append( point ) # Keep a list of all 3D points for the sub so that we can connect them correctly
                if noDex in specialNodes: # store according to whether it is special or not
                    specialR3.append( point )
                else:
                    nodesR3.append( point )
            for noDex , node in enumerate(currNodes): # for each of the nodes in this subgraph
                # ~~ Edges ~~
                for edge in node.edges:
                    if (edge.weight == 'transfer') and (not edge.tag in transfers):
                        tagPair = ( edge.tail.tag , edge.head.tag )
                        coordPair = ( allR3[nodeHash[edge.tail.tag]] , allR3[nodeHash[edge.head.tag]] )
                        if pair_in_list( tagPair , specialEdges ):
                            edgePairListSpecialR3.append( coordPair )
                        else:
                            edgePairListR3.append( coordPair ) # Store edge endpoints for drawing
                        transfers.add( edge.tag ) # Mark this edge as drawn, both Nodes should have a reference to the same Edge no matter where they are
#            if specialR3:
#                ptFromLast = specialR3[-1] # This is the last special point encountered this for
#            else:
#                print level, ", No special 3D point were collected!"
            
            if ptFromLast:
                print "Transition points last to current", (ptFromLast , specialR3[0] )
                edgePairListSpecialR3.append( (ptFromLast , allR3[find_tag_in_node_list( currNodes , action[1][0] )] ) )
            
            xs,ys,zs = split_to_components( specialR3 ) # What size should the markers be?
            ax.scatter(xs, ys, zs, c=specialColor, s=140)
            xs,ys,zs = split_to_components( nodesR3 )
            ax.scatter(xs, ys, zs, c=plainColor)
            
            print len(edgePairListR3), "nonspecial edges  ,",
            for edge in edgePairListR3:
                xs,ys,zs = split_to_components( edge )
                ax.plot(xs, ys, zs, c=plainColor)
                
            print len(edgePairListSpecialR3), "special edges"
            for edge in edgePairListSpecialR3:
                xs,ys,zs = split_to_components( edge )
                ax.plot(xs, ys, zs, c=specialColor, linewidth=3)
            
            # 3.d. If it is a transition, get the destination subgraph, Paint destination node special, store special
            if action[0] == 'transition':
                ptFromLast = allR3[ find_tag_in_node_list( currNodes , action[1][0] ) ]
                zDex += 1
                print "Inrement zDex to", zDex
            else:
                ptFromLast = None
                
            currSub = action[4]
            currNodes = currSub.nodes.item_list() # Get all the nodes so that we can iterate through
            nodeHash = hash_TagObj_list(currNodes) # Relate tags to indices so that we can connect them correctly
            # Generate points around a circle
            numPts = len(currNodes)
            subPts = circ_spacing( circDiam , numPts, flatCenter) # Get the node locations for this subgraph
            # 2. Paint initial node special, store special
            specialNodes = [] # A list of indices that are painted a special color
            specialEdges = [] # A list of edges that are painted a special color
            
            lastAction = action # store this edge type for the next loop
        
        plot_axes_3D_mpl(ax, scale = 0.05) 
        plt.gca().set_aspect('equal')
        plt.show()
        
  


# == End Batch Processing ==
  
# == Slide Show ==
  

#dbgLog(1, "DEBUG: Load success!", PKLpath )          
#dbgLog(0, 'This file contains', len(procMesh['approvedGraspPairs']), 'approved grasp pairs, as follows', procMesh['approvedGraspPairs'] )
  
spamSTLPath = "pssn_stl/Spam12oz.stl" # A simple representation of the hand
pklPointsPath = "output/Spam12oz_uniq-pts.pkl"

#if not os.path.isfile(pklPointsPath): # if a points-only file has not already been generated, then generate it
#    #hand_mesh = mesh.Mesh.from_file(handSTLPath) 
#    uniq_pts_from_stl(spamSTLPath, pklToFile = True) # Load the STL file and pickle as a points-only file
#
#spamPoints = load_pkl_struct(pklPointsPath) # load the generated file

# plot_points_only_model(pklPointsPath) # Plot the points of the STL model
# plot_clusters(procMesh) # plot the clusters as color-coded points
# plot_clusters_with_stability(procMesh, divisor = 5) # Plot the center of mass projected onto clusters

#for PKLpath in ["output/FrenClasYellMust14oz.pkl",
#                "output/MeliDougFarmFresFruiStra.pkl",
#                "output/MastChefGrouCoff297g.pkl",
#                "output/ExpoBlacDryErasMarkFine.pkl",
#                "output/DarkRedFoamBlocWithThreHole.pkl"]: #= 'output/Spam12oz.pkl' # load a processed mesh for this test
#    procMesh = load_processed_mesh(PKLpath) # load a processed mesh
#    plot_opposing_grasp_pnts(procMesh, scale = 0.02, divisor = 5)

# == End Show ==




# foo = load_processed_mesh('output/StaiSteeSpooRedHand.pkl') 
# foo = load_processed_mesh('output/BlocOfWood6in.pkl')

#plot_clusters(your_mesh)
#plot_clusters_with_tiling(your_mesh)
#plot_clusters_with_stability(your_mesh, divisor = 10)
#plot_opposing_grasp_pnts(your_mesh, scale = 0.02, divisor = 20)

# == End Point Demo ==
 
 
 
# FIXME: THE FOLLOWING IS AWFUL
""" # SOMETHING ISN'T RIGHT WITH THIS CODE, FINDING 43409 GRASP PAIRS
# 2016-03-17: FOR NOW I HAVE TO CONTINUE WITH THE GRASP POINT QUALITY AS IT STANDS SO THAT I CAN MAKE PROGRESS, TEST LATER

# Now that suitable pairs of clusters have been located, need to choose between the tiled points on those pairs for candidate grasps
    
# Some of the clusters formed from the STL models can be very small, in these cases it should be adequate to just choose 
#the center of each selected cluster as a candidate grasp point.  If the entire cluster is no wider than the gripper finger,
#there is no reason to consider the tiling of points on the tiny cluster.  Just choose the centroid of the cluster.

# All the candidate (non-centroid) grasp points have been generated already.  Do not generate non-centroid points. Simply
#indicate the indices of points that we will care about. Indices of candidates used for gripping might be different for
#different pairs that share the same clusters. Therefore pairs of indices must match pairs of suitable clusters

#tinyClusters = set([]) # init empty set of clusters too small for consideration of tiling
    
for n, pair in enumerate(opposingPairsByProjection): # for each pair of suitable clusters
    indicesPair = [None,None]
    for pairDex, clusDex in enumerate(pair): # for each index in the suitable pair
        if not (clusDex in tinyClusters): # if the cluster index has not been marked as tiny
            # Check if the cluster should be marked as tiny
            markTiny = False
            xSpan = abs(flatBoxes[clusDex][1][0] - flatBoxes[clusDex][0][0]) # x-extent of the flattened cluster bounding box
            ySpan = abs(flatBoxes[clusDex][1][1] - flatBoxes[clusDex][0][1]) # y-extent of the flattened cluster bounding box
            markTiny = (xSpan <= fingerPadDia) and (ySpan <= fingerPadDia)
            if markTiny: # if cluster is tiny, then
                tinyClusters.add( clusDex ) # mark cluster tiny
                area , centroid = poly2D_area_centroid( flatEdges[clusDex] ) # Calc centroid
                centroid3D = np.add( flatBases[clusDex]['origin'], # Transform centroid to STL basis
                                     np.add( np.multiply( flatBases[clusDex]['xVec'] , centroid[0] ) ,      
                                             np.multiply( flatBases[clusDex]['yVec'] , centroid[1] ) ) )
                trialGraspPoints[clusDex].append( centroid3D ) # Append centroid to the tiling
                #indicesPair[pairDex] = [ len(trialGraspPoints[clusDex])-1 ] # Assume that the last index in the tiling is the centroid
            # else cluster is not tiny, await further processing
        # else cluster has already been marked tiny and centroid located, no action
    
    # Entries in 'approvedGraspPairs' take the form:
    # ( ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ),  # side 0 of opposing pair
    #   ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ) ) # side 1 of opposing pair
    
    i = pair[0]
    j = pair[1]
    if (i in tinyClusters) and (j in tinyClusters): # If both clusters are tiny, there is only one pair of points generated
        approvedGraspPairs.append( ( ( i , len(trialGraspPoints[i])-1 ) , ( j , len(trialGraspPoints[j])-1 ) ) )
    else: # else at least one of the clusters are not tiny, an evaluation must be made of the tiling of the non-tiny clusters
        Ibasis = flatBases[i]
        Jbasis = flatBases[j]
        if i in tinyClusters:
            Irange = [len(trialGraspPoints[i])-1]
        else:
            Irange = range(len(trialGraspPoints[i]))
        if j in tinyClusters:
            Jrange = [len(trialGraspPoints[j])-1]
        else:
            Jrange = range(len(trialGraspPoints[j]))
        # Assume that the centroid of a cluster is in the middle of a flat spot
        area , centroidI = poly2D_area_centroid( flatEdges[i] ) # Calc centroid # Should I have already done this for every cluster?
        area , centroidJ = poly2D_area_centroid( flatEdges[j] ) # Calc centroid
        Icentroid3D = np.add( flatBases[i]['origin'], # Transform centroid to STL basis
                              np.add( np.multiply( flatBases[i]['xVec'] , centroidI[0] ) ,      
                                      np.multiply( flatBases[i]['yVec'] , centroidI[1] ) ) )
        Jcentroid3D = np.add( flatBases[j]['origin'], # Transform centroid to STL basis
                              np.add( np.multiply( flatBases[j]['xVec'] , centroidJ[0] ) ,      
                                      np.multiply( flatBases[j]['yVec'] , centroidJ[1] ) ) )
        spanI = max( abs(flatBoxes[i][1][0] - flatBoxes[i][0][0]) , # x-extent of the flattened cluster bounding box I
                     abs(flatBoxes[i][1][1] - flatBoxes[i][0][1]) ) # y-extent of the flattened cluster bounding box I
        spanJ = max( abs(flatBoxes[j][1][0] - flatBoxes[j][0][0]) , # x-extent of the flattened cluster bounding box J
                     abs(flatBoxes[j][1][1] - flatBoxes[j][0][1]) ) # y-extent of the flattened cluster bounding box J                                         
        pairScores = PriorityQueue() # Structure to rank pairs of grasps
        lastPointI = [infty,infty,infty] # init last point to infinity
        lastPointJ = [infty,infty,infty] # init last point to infinity
        for iTrialDex in Irange:
            for jTrialDex in Jrange:
                # For every possible pairing of trial points on the two clusters, calculate a score and push onto queue
                pointI = trialGraspPoints[i][iTrialDex]
                pointJ = trialGraspPoints[j][jTrialDex]
                pointAwayI = np_unit_vec( np.subtract( pointI , pointJ ) )
                pointAwayJ = np_unit_vec( np.subtract( pointJ , pointI ) )
                angleI = np_angle_between(pointAwayI, clusterNorms[i])
                angleJ = np_angle_between(pointAwayJ, clusterNorms[j])
                distI = sci_spatial.distance.euclidean( pointI , Icentroid3D )
                distJ = sci_spatial.distance.euclidean( pointJ , Jcentroid3D )
                # Should I be storing these scores so that I can operate on them later for choosing grasps?
                scoreI = (normalTolerance - angleI) / (2*normalTolerance) + distI / (2*spanI)
                scoreJ = (normalTolerance - angleJ) / (2*normalTolerance) + distJ / (2*spanJ)
                # URL, Putting items in a PriorityQueue so that they will be retrieved highest first: http://stackoverflow.com/a/15124115/893511
                pairScores.put( 
                    ( -(scoreI+scoreJ), 
                       ( ( i , iTrialDex ) , ( j , jTrialDex ) ) ) 
                )
        print "There are"
        # Pop pairs of grasps one by one
        while not pairScores.empty():
            currPair = pairScores.get()[1]
            currI = trialGraspPoints[currPair[0][0]][currPair[0][1]]
            currJ = trialGraspPoints[currPair[1][0]][currPair[1][1]]
            if (sci_spatial.distance.euclidean( currI , lastPointI ) > fingerPadDia) and \
               (sci_spatial.distance.euclidean( currJ , lastPointJ ) > fingerPadDia):
                   # Entries in 'approvedGraspPairs' take the form:
                   # ( ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ),  # side 0 of opposing pair
                   #   ( cluster index of trialGraspPoints , point index of trialGraspPoints[cIndex] ) ) # side 1 of opposing pair
                   approvedGraspPairs.append( currPair )
                   lastPointI = currI
                   lastPointJ = currJ
"""

# == Abandoned Code ==

"""
def facet_stability(COM, flatBases, flatEdges):
    #COM = spamStruct['volumeCentroid']
    #flatBases = spamStruct['flatBases']
    #flatEdges = spamStruct['edges2D']
    facetStability = []
    
    for i, facet in enumerate(flatEdges):
        facetStability.append( None ) # append a default failed stability assessment
        if len(facet) > 0: # If there is a flattened cluster to operate on, perform an assessment of its suitability to support object
            # Project the COM onto the facet
            facetOrigin = flatBases[i]['origin'] 
            transformedCOMX = np_vec_proj(np.subtract(COM , facetOrigin), flatBases[i]['xVec']) 
            transformedCOMY = np_vec_proj(np.subtract(COM , facetOrigin), flatBases[i]['yVec'])
            flatCOM = [transformedCOMX , transformedCOMY]
            if point_in_poly_w( flatCOM , flatEdges[i] ): # the point is inside the flattened hull
                projectedInside = True # flag this as a potentially stable facet to rest the object on
            else:
                projectedInside = False # The COM was not projected to the interior of the current facet
            # Compute the closest distance to an edge of the facet
            shortestDist = infty
            for segDex in range(1, len(facet)): # for every line segment in the facet, comput distance from project COM to segment
                temp = d_point_to_segment_2d( flatCOM , facet[segDex] , facet[segDex-1] )
                if temp < shortestDist: 
                    shortestDist = temp # Store the shortest distance to any segment
            facetStability[-1] = {'projectedCOM': np.add( flatBases[i]['origin'], # COM projected onto cluster                                         
                                                          np.add( np.multiply( flatBases[i]['xVec'] , transformedCOMX ) ,      
                                                                  np.multiply( flatBases[i]['yVec'] , transformedCOMY ) ) ) , 
                                       'flatCOM': flatCOM, # ---------------------- COM projected onto flattened cluster, in that cluster basis
                                 'projCOMinside': projectedInside, # -------------- Flag for whether the projected COM was within the cluster edge
                                'lestDistToEdge': shortestDist} # ----------------- Shortest distance from the projected COM to nearest edge
            #print "DEBUG: Angle b/n facet normal and line chosen [rad]:",np_angle_between(flatBases[i]['zVec'] , np.subtract(facetStability[-1]['projectedCOM'], flatBases[i]['origin'] ) )
            #testAngle = np_angle_between(flatBases[i]['zVec'] , np.subtract(facetStability[-1]['projectedCOM'], COM ) )
            #print testAngle,'\t',eq(testAngle, 0) or eq(testAngle,np.pi)#,'\t',
            # A line from the COM to the projected COM is reasonably perpendicular to the associated cluster
    return facetStability

"""

# == End Abandoned ==