== Structures ==

{
    'mesh' :              mesh.Mesh object generated from 'meshPath'
    'clusters' :          List of STL points that have been flattened onto clusters
    'clusterNorms' :      List of normal vectors for each of the clusters in 'clusters'
    'flatBases' :         Dictionary of basis vectors for each of the clusters, with the X-Y plane aligned with the cluster plane
    'flatBoxes' :         Bounding box of clustered points expressed in 'flatBases': ( ( cXmin , cYmin ) , ( cXmax , cYmax ) )
    'clusterEdges' :      List of cluster edges, each edge is a list of ordered points in the STL bases
    'edges2D' :           List of cluster edges, each edge is a list of ordered points in 'flatBases'
    'trialGraspPoints' :  List of trial grasp tilings, each is a list of 3D points associated with a cluster
                          2016-04-28: As of right now, any cluster that is part of an approved grasp pair will have the 
                                      centroid of the of the cluster appended to the list of trial points associated with 
                                      the cluster. Assume that the last element is the point to grasp / place a finger at
    'volumeCentroid' :    3D point signifying the volume centroid of the 'mesh'. Stand-in for COM assuming uniform desnity
    'clusterStability':   Dictionary with information about whether or not a cluster is an appropriate resting face for a pose
    'approvedGraspPairs': List of tuples denoting items of 'trialGraspPoints' that are suitable for grasping given criteria
}

# flatBases
{ 
   'origin': xTail, 
  'orgXDir': xHead,         
     'xVec': xVec,          
     'yVec': yVec,          
     'zVec': clusterNorms[i],
  'xOffset': pOffset 
}

# clusterStability
{'projectedCOM': [x,y,x] # ------------------ COM projected onto the 3D cluster
 'flatCOM': flatCOM, # ---------------------- COM projected onto flattened cluster, in that cluster basis
 'projCOMinside': projectedInside, # -------------- Flag for whether the projected COM was within the cluster edge
 'lestDistToEdge': shortestDist} # ----------------- Shortest distance

#flatBoxes
( ( cXmin , cYmin ) , ( cXmax , cYmax ) )

== Other ==

np.add( flatBases[i]['origin'], # COM projected onto cluster                                         
        np.add( np.multiply( flatBases[i]['xVec'] , transformedCOMX ) ,      
                np.multiply( flatBases[i]['yVec'] , transformedCOMY ) ) )


